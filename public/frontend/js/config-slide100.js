(function ($) {
    "use strict";
    // Use Strict
    $('#slide100-01').slide100({
        autoPlay: "false",
        timeAuto: 3000,
        deLay: 400,

        linkIMG: [
            'images/pro-detail-01.jpg',
            'images/pro-detail-02.jpg',
            'images/pro-detail-01.jpg',
            'images/pro-detail-02.jpg',
            'images/pro-detail-01.jpg',
        ],

        linkThumb: [
             'images/pro-detail-01.jpg',
            'images/pro-detail-02.jpg',
            'images/pro-detail-01.jpg',
            'images/pro-detail-02.jpg',
            'images/pro-detail-01.jpg',
        ]
    });
})(jQuery);