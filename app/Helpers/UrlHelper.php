<?php
use Illuminate\Support\Str;

if (! function_exists('baseUrl')) {
    function baseUrl($path = 'web')
    {
        switch ($path) {
            case 'admin':
                return config('app.url') . adminPath();
                break;
            case 'supplier':
                return config('app.url') . supplierPath();
                break;
            case 'shop':
                return config('app.url') . shopPath();
                break;
            default:
                return config('app.url') . webPath();
        }
    }
}

if (! function_exists('webPath')) {
    function webPath($path = '')
    {
        return config('app.url_web_path')  . Str::start(request()->shop->no, '/') . ($path ? Str::start($path, '/'): '');
    }
}

if (! function_exists('adminPath')) {
    function adminPath($path = '')
    {
        return config('app.url_admin_path') . ($path ? Str::start($path, '/'): '');
    }
}

if (! function_exists('supplierPath')) {
    function supplierPath($path = '')
    {
        return config('app.url_supplier_path') . ($path ? Str::start($path, '/'): '');
    }
}

if (! function_exists('shopPath')) {
    function shopPath($path = '')
    {
        return config('app.url_shop_path') . ($path ? Str::start($path, '/'): '');
    }
}
