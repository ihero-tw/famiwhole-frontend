<?php
use Illuminate\Support\Str;

if (! function_exists('subdirAsset')) {
    function subdirAsset($path, $mod = 'web')
    {
        if (env('APP_ENV') != 'local') {
            if ($mod == 'supplier') {
                $prefix = config('app.url_supplier_path');
            } elseif ($mod == 'admin') {
                $prefix = config('app.url_admin_path');
            } elseif ($mod == 'shop') {
                $prefix = config('app.url_shop_path');
            } else {
                $prefix = config('app.url_web_path');
            }
            $path = Str::finish($prefix, $path);
        }
        $path = Str::finish($path, '?') . date('YmdH');

        return asset($path);
    }
}

if (! function_exists('subdirMix')) {
    function subdirMix($path, $mod = 'web')
    {
        $prefix = '';
        if (env('APP_ENV') != 'local') {
            if ($mod == 'supplier') {
                $prefix = config('app.url_supplier_path');
            } elseif ($mod == 'admin') {
                $prefix = config('app.url_admin_path');
            } elseif ($mod == 'shop') {
                $prefix = config('app.url_shop_path');
            } else {
                $prefix = config('app.url_web_path');
            }
        }

        $pathArray = array_merge(
            explode('/', $prefix),
            explode('/', mix($path))
        );
        $pathArray = array_filter($pathArray);
        $path = implode('/', $pathArray);
        $path = Str::finish($path, '?') . date('YmdH');

        return Str::start($path, '/');
    }
}

if (! function_exists('subdirRoute')) {
    function subdirRoute($path, $mod = 'web')
    {
        $prefix = '';
        if (env('APP_ENV') != 'local') {
            if ($mod == 'supplier') {
                $prefix = config('app.url_supplier_path');
            } elseif ($mod == 'admin') {
                $prefix = config('app.url_admin_path');
            } elseif ($mod == 'shop') {
                $prefix = config('app.url_shop_path');
            } else {
                $prefix = config('app.url_web_path');
            }
        }

        $pathArray = array_merge(
            explode('/', $prefix),
            explode('/', $path)
        );
        $pathArray = array_filter($pathArray);
        $path = implode('/', $pathArray);

        return filled($path)? Str::start($path, '/'): '';
    }
}
