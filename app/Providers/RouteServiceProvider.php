<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/dashboard';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        Route::pattern('identity', '[a-z0-9.\-]+');

        Route::bind('shop', function ($value) {
            $shop = new \stdClass();
            $shop->no = $value;
            $shop->name = '夏日居家遊樂場';
            $shop->icon = 'https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/favicon.png';
            $shop->logo = 'https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/files/2b7c2350-6fb7-4f86-8923-ac433d35af94';
            $shop->intro = '萬豪資訊，將客戶的問題視為自己的問題，將會提出不同的解決方案，我們不只是將事情做到更要做對做好。';
            $shop->contact = json_decode('{
                "phone": "04-22583569",
                "email": "service@fortune-inc.com",
                "address": {
                    "zipcode": "100",
                    "city": "臺北市",
                    "town": "中正區",
                    "street": "二路二段二樓"
                }
            }');
            $shop->social_medias = json_decode('{
                "links": [
                    {
                        "type": "facebook",
                        "action": 1,
                        "uri": "https://www.facebook.com/FamilyMart/posts/10154858278894258/"
                    },
                    {
                        "type": "line",
                        "action": 1,
                        "uri": "line://ti/p/@883pxdva"
                    },
                    {
                        "type": "instagram",
                        "action": 1,
                        "uri": "https://instagram.com/p/BM8HEUXDg3_/"
                    },
                    {
                        "type": "youtube",
                        "action": 1,
                        "uri": "https://www.youtube.com/channel/UCE8xBijF6cJmfsOVTn8HJCQ"
                    }
                ],
                "plugin": {
                    "type": "none",
                    "uri": "https://www.facebook.com/ContentMarketingTW/"
                }
            }');
            $shop->markets = collect([
                [
                    'no' => '5157424d',
                    'name' => '所有商品',
                    'active' => 1,
                    'intro' => '<p>這裡是可以放 html 的文案內容。<img src=images/inner-banner.jpg></p>'
                ],
                [
                    'no' => '59784767',
                    'name' => '蘋果熱銷品推薦',
                    'active' => 1,
                    'intro' => '<p>這裡是可以放 html 的文案內容。<img src=images/inner-banner.jpg></p>'
                ],
                [
                    'no' => '4c641da7',
                    'name' => '新品上架',
                    'active' => 1,
                    'intro' => '<p>這裡是可以放 html 的文案內容。<img src=images/inner-banner.jpg></p>'
                ],
                [
                    'no' => '1d39s004',
                    'name' => 'iPhone保護貼',
                    'active' => 1,
                    'intro' => '<p>這裡是可以放 html 的文案內容。<img src=images/inner-banner.jpg></p>'
                ],
                [
                    'no' => '4s30058',
                    'name' => 'iPhone保護殼',
                    'active' => 1,
                    'intro' => '<p>這裡是可以放 html 的文案內容。<img src=images/inner-banner.jpg></p>'
                ],
                [
                    'no' => '984ad31',
                    'name' => 'iPad保護貼/保護殼/平板配件',
                    'active' => 1,
                    'intro' => '<p>這裡是可以放 html 的文案內容。<img src=images/inner-banner.jpg></p>'
                ],
                [
                    'no' => '8471kae3',
                    'name' => 'AirPods配件',
                    'active' => 1,
                    'intro' => '<p>這裡是可以放 html 的文案內容。<img src=images/inner-banner.jpg></p>'
                ],
                [
                    'no' => '85710i31',
                    'name' => 'Apple Watch配件',
                    'active' => 1,
                    'intro' => '<p>這裡是可以放 html 的文案內容。<img src=images/inner-banner.jpg></p>'
                ],
            ])->map(function ($item) {
                $market = new \stdClass();
                $market->no = $item['no'];
                $market->name = $item['name'];
                $market->intro = $item['intro'];
                return $market;
            });

            return $shop;
        });
        Route::bind('market', function ($value) {
            $market = new \stdClass();
            $market->no = $value;
            $market->name = '所有商品';
            $market->intro = '<p>這裡是可以放 html 的文案內容。<img src=images/inner-banner.jpg></p>';

            return $market;
        });

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->group(base_path('routes/api.php'));

            Route::prefix(config('app.url_admin_path'))
                ->name('admin.')
                ->middleware('admin')
                ->group(base_path('routes/admin.php'));

            Route::prefix(config('app.url_supplier_path'))
                ->name('supplier.')
                ->middleware('supplier')
                ->group(base_path('routes/supplier.php'));

            Route::prefix(config('app.url_shop_path'))
                ->name('shop.')
                ->middleware('shop')
                ->group(base_path('routes/shop.php'));

            Route::prefix(config('app.url_web_path'))
                ->name('web.')
                ->middleware('web')
                ->group(base_path('routes/web.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
