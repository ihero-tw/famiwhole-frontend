<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SupportServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('cart', 'App\Support\Cart');
        $this->app->bind('cart.items', 'App\Support\CartItems');
    }
}
