<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Request::macro('isAdmin', function () {
            return Str::contains(Str::start($this->path(), '/'), adminPath());
        });
        Request::macro('isSupplier', function () {
            return Str::contains(Str::start($this->path(), '/'), supplierPath());
        });
        Request::macro('isShop', function () {
            return Str::contains(Str::start($this->path(), '/'), shopPath());
        });
        Request::macro('isWeb', function () {
            return Str::contains(Str::start($this->path(), '/'), webPath());
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
