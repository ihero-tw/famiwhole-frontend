<?php

namespace App\Support;

use App\Support\CartItems;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Collection;

class Cart
{
    const IDENTIFICATION_CODE = 'FAMILYMART';

    /**
     * Instance of the cache manager.
     *
     * @var \Illuminate\Session\SessionManager
     */
    private $cache;

    /**
     * Holds the current cart instance.
     *
     * @var string
     */
    private $identity;

    /**
     * Cart constructor.
     *
     * @param \Illuminate\Session\SessionManager $cache
     */
    public function __construct(SessionManager $cache)
    {
        $this->cache = $cache;
        $this->identity = $this->getIdentity();
    }

    /**
     * Set the current cart instance.
     *
     * @param string $uuid
     *
     * @return boolean
     */
    // public function load(String $uuid)
    // {
    //     $this->identity = $this->getIdentity($uuid);

    //     return $this;
    // }

    /**
     * Get the carts cache key.
     *
     * @return String
     */
    protected function getIdentity()
    {
        return sprintf('%s.%s', 'cart', self::IDENTIFICATION_CODE);
    }

    /**
     * Get cart all items.
     *
     * @return \Illuminate\Support\Collection
     */
    public function all()
    {
        $content = $this->getContent();

        return $content->get('items', collect([]))->sortKeys();
    }

    /**
     * Put a item to cart.
     *
     * @return \Illuminate\Support\Collection
     */
    public function put(CartItems $item, Int $qty)
    {
        $content = $this->getContent();

        $items = $content->get('items', collect([]));
        if ($items->has($item->identity)) {
            $item = $items->pull($item->identity);
        }

        if (filled($qty)) {
            $item->qtyAddtion($qty);
        }

        $items->put($item->identity, $item);

        $this->saveContent($content->replace([
            'items' => $items
        ]));
    }

    /**
     * Update a item data.
     *
     * @return \Illuminate\Support\Collection
     */
    public function update(String $identity, Int $qty)
    {
        $content = $this->getContent();

        $items = $content->get('items', collect([]));
        if ($items->has($identity)) {
            $item = $items->pull($identity);
            $item->qty($qty);

            $this->saveContent($content->replace([
                'items' => $items->put($identity, $item)
            ]));
        }
    }

    /**
     * Replace a item data.
     *
     * @return \Illuminate\Support\Collection
     */
    public function replace(String $identity, Int $qty, CartItems $item)
    {
        $content = $this->getContent();

        $items = $content->get('items', collect([]));
        if ($items->has($identity)) {
            $items->forget($identity);
        }

        $item->qty($qty);

        $this->saveContent($content->replace([
            'items' => $items->put($item->identity, $item)
        ]));
    }

    /**
     * Put a item to cart.
     *
     * @return Object
     */
    public function remove(String $identity)
    {
        $content = $this->getContent();

        $items = $content->get('items', collect([]));
        if ($items->has($identity)) {
            $items->forget($identity);

            $this->saveContent($content->replace([
                'items' => $items
            ]));
        }
    }

    /**
     * Undocumented function
     *
     * @param String $identity
     * @return Object|Null
     */
    public function pull(String $identity)
    {
        $content = $this->getContent();

        $items = $content->get('items', collect([]));
        if ($items->has($identity)) {
            $data = $items->pull($identity);

            $this->saveContent($content->replace([
                'items' => $items
            ]));

            return $data;
        }
        return null;
    }

    /**
     * Undocumented function
     *
     * @param String $id
     * @return Boolean
     */
    public function exist(String $identity)
    {
        $content = $this->getContent();

        $items = $content->get('items', collect([]));

        return $items->has($identity);
    }

    /**
     * Undocumented function
     *
     * @return boolean
     */
    public function isEmpty()
    {
        return blank($this->getContent());
    }

    /**
     * Undocumented function
     *
     * @return Boolean
     */
    public function destroy()
    {
        return $this->cache->forget($this->identity);
    }

    /**
     * Get the carts content, if there is no cart content set yet, return a new empty Collection
     *
     * @return \Illuminate\Support\Collection
     */
    protected function getContent()
    {
        $content = $this->cache->has($this->identity)
            ? $this->cache->get($this->identity)
            : collect([]);

        return $content;
    }

    /**
     * Save cart content.
     *
     * @param Collection $content
     * @return void
     */
    protected function saveContent(Collection $content)
    {
        $this->cache->put($this->identity, $content);
    }
}
