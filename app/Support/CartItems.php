<?php
namespace App\Support;

use Illuminate\Database\Eloquent\Model;

class CartItems
{
    /**
     * CampaignItem Model.
     *
     * @var Model
     */
    private $item;

    /**
     * Holds the current cart instance.
     *
     * @var string
     */
    public $identity;

    /**
     * The quantity for this cart item.
     *
     * @var int|float
     */
    public $qty = 0;

    /**
     * CartItem constructor.
     *
     * @param Products|ProductModel $item
     */
    public function __construct($item)
    {
        $this->item = $item;

        $this->generateIdentity();
    }

    /**
     * Generate item identity.
     *
     * @return void
     */
    protected function generateIdentity()
    {
        $this->identity = bin2hex(
            sprintf('%s-%s', $this->product->id, optional($this->item)->id ?? '')
        );
    }

    /**
     * Undocumented function
     *
     * @param Int $qty
     * @return void
     */
    public function qty(Int $qty)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param Int $qty
     * @return void
     */
    public function qtyAddtion(Int $qty)
    {
        $this->qty += $qty;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function fresh()
    {
        $this->item = $this->item->fresh();

        return $this;
    }

    /**
     * Get an attribute from the cart item or get the campaign items model.
     *
     * @param string $attribute
     * @return mixed
     */
    public function __get($attribute)
    {
        if (isset($this->item)) {
            return $this->item->{$attribute};
        }

        return null;
    }
}
