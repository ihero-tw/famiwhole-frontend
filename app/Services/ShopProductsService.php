<?php

namespace App\Services;

use App\Support\CartItems;
use Illuminate\Http\Request;

class ShopProductsService
{
    /**
     * Generate object item of cart and put the cart.
     *
     * @param Request $request
     * @return Object
     */
    public function generateCartItem(Request $request)
    {
        $product = new \stdClass();
        $product->id = 1;
        $product->name = '小米漢堡';
        $product->medias = 'https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/4673bb9c-e2f7-4647-953c-76e849f059fc';

        $productModel = new \stdClass();
        $productModel->id = 1;
        $productModel->option_uuids = '5157424d597847674c64-43505359565a654a6758';
        $productModel->name = '紅米 / 恐龍肉';
        $productModel->price_special = 100;
        $productModel->product = $product;

        return new CartItems($productModel);
    }
}
