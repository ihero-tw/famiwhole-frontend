<?php

namespace App\Services;

use App\Support\CartItems;
use App\Support\Facades\Cart;
use Illuminate\Http\Request;
use App\Exceptions\RuntimeException;

class CartService
{
    /**
     * __construct
     */
    public function __construct()
    {
        //
    }

    /**
     * Put item with quantity to cart.
     *
     * @param CartItems $item
     * @param Request $request
     * @return void
     */
    public function putItem(CartItems $item, Request $request)
    {
        Cart::put($item, $request->input('item.qty'));
    }

    /**
     * Put item with quantity to cart by batch.
     *
     * @param Array $items
     * @return Object
     */
    public function putItemsBatch(Array $items)
    {
        foreach ($items as $item) {
            Cart::put($item['content'], $item['qty']);
        }
    }

    /**
     * Update quantity of item by id.
     *
     * @param Request $request
     * @return Object
     */
    public function updateQty(String $identity, Int $qty)
    {
        Cart::update($identity, $qty);
    }

    /**
     * Update quantity of item by id.
     *
     * @param Array $items
     * @return Object
     */
    public function updateQtyBatch(Array $items)
    {
        foreach ($items as $item) {
            Cart::update($item['id'], $item['qty']);
        }
    }

    /**
     * Replace item.
     *
     * @param CartItems $item
     * @param Request $request
     * @return void
     */
    public function replaceItem(String $identity, Int $qty, CartItems $item)
    {
        Cart::replace($identity, $qty, $item);
    }

    /**
     * Remove item for cart.
     *
     * @param String $identity
     * @return Object
     */
    public function removeItem(String $identity)
    {
        Cart::remove($identity);
    }

        /**
     * Remove item for cart.
     *
     * @param String $identity
     * @return Object
     */
    public function removeItems(Array $items)
    {
        foreach ($items as $item) {
            Cart::remove($item['identity']);
        }
    }

    /**
     * Get listing of item.
     *
     * @return void
     */
    public function getItems()
    {
        $items = Cart::all();
        foreach ($items as $item) {
            $item->fresh();
        }

        return $items;
    }

    /**
     * Destroy new cart.
     *
     * @return Boolean
     */
    public function destroy()
    {
        return Cart::destroy();
    }

    /**
     * Destroy new cart.
     *
     * @return Boolean
     */
    public function isEmpty()
    {
        if (blank(Cart::all())) {
            throw new RuntimeException('購物車是空的');
        }
    }

    /**
     * Load specify cart.
     *
     * @param String $uuid
     * @return void
     */
    public function loadCart(String $uuid)
    {
        Cart::load($uuid);

        return $this;
    }
}
