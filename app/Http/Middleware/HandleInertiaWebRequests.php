<?php

namespace App\Http\Middleware;

use App\Http\Resources\ShopResource;
use App\Http\Resources\MarketResource;
use App\Http\Resources\ShoppingCartPreviewResource;
use App\Support\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Inertia\Middleware;

class HandleInertiaWebRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     * @var string
     */
    protected $rootView = 'web.layouts.app';

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    /**
     * Defines the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request)
    {
        return array_merge(parent::share($request), [
            'data' => $request->session()->get('data') ?? null,
            'shop' => new ShopResource($request->shop),
            'markets' => MarketResource::collection($request->shop->markets),
            'cart' => [
                'items' => !Cart::isEmpty()? ShoppingCartPreviewResource::collection(Cart::all()->values()): []
            ],
            'flash' => function () use ($request) {
                return [
                    'alert' => array(
                        'success' => $request->session()->get('alert.success'),
                        'error' => $request->session()->get('alert.error'),
                    ),
                    'notify' => array(
                        'success' => $request->session()->get('notify.success'),
                        'warning' => $request->session()->get('notify.warning'),
                        'error' => $request->session()->get('notify.error'),
                    )
                ];
            },
            'routeName' => Route::currentRouteName(),
            'backurl' => url()->previous()
        ]);
    }
}
