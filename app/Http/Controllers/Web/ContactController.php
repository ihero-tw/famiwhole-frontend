<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Exceptions\RuntimeException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Response;
use App\Http\Resources\MarketResource;
class ContactController extends Controller
{
    public function __construct() {
        //
    }

    public function index($shop)
    {
        return Inertia::render('contact/contact', [
        ]);
    }
}
