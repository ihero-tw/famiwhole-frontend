<?php

namespace App\Http\Controllers\Web;

use App\Http\Resources\MarketResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

class MarketsController extends Controller
{
    /**
     * __construct.
     */
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the accounts.
     *
     * @return Response
     */
    public function index($shop)
    {
        return Inertia::render('Home/Index', [
            'market' => new MarketResource($shop->markets->first()),
            'banner' => json_decode($this->getBannerData()),
            'products' => json_decode($this->getProductData())
        ]);
    }

    public function getProductData()
    {
        return '{
            "data": [
                {
                    "id": 1,
                    "no": "2021080400001",
                    "name": "小米漢堡",
                    "price": "$100 ~ $10",
                    "intro": "商品簡介, 放在價格下面的文案",
                    "description": "<p>產品介紹，要可以放 HTML 內容</p>",
                    "recommended": "<p>推薦分享，要可以放 HTML 內容</p>",
                    "shippingMethods": [
                        {
                            "id": 1,
                            "name": "全家取貨付款"
                        }
                    ],
                    "paymentMethods": [
                        {
                            "id": 1,
                            "name": "全家店到店"
                        }
                    ],
                    "categories": [
                        {
                            "id": 1,
                            "name": "彩妝類",
                            "type": "big"
                        },
                        {
                            "id": 2,
                            "name": "彩妝用品",
                            "type": "medium"
                        },
                        {
                            "id": 3,
                            "name": "指甲彩繪",
                            "type": "small"
                        }
                    ],
                    "models": [
                        {
                            "id": 1,
                            "uuid": "5157424d597847674c64-43505359565a654a6758",
                            "uuids": [
                                "5157424d597847674c64",
                                "43505359565a654a6758"
                            ],
                            "name": "紅米 / 恐龍肉",
                            "sku": "GB001",
                            "price": {
                                "original": 200,
                                "special": 100
                            },
                            "stockSurplus": 10
                        },
                        {
                            "id": 2,
                            "uuid": "5157424d597847674c65-43505359565a654a6758",
                            "uuids": [
                                "5157424d597847674c65",
                                "43505359565a654a6758"
                            ],
                            "name": "黑米 / 恐龍肉",
                            "sku": "GB002",
                            "price": {
                                "original": 200,
                                "special": 10
                            },
                            "stockSurplus": 10
                        }
                    ],
                    "spec": {
                        "type": "double",
                        "list": [
                            {
                                "name": "外皮",
                                "options": [
                                    {
                                        "id": 1,
                                        "uuid": "5157424d597847674c64",
                                        "name": "紅米",
                                        "sort": 1
                                    },
                                    {
                                        "id": 2,
                                        "uuid": "5157424d597847674c65",
                                        "name": "黑米",
                                        "sort": 2
                                    }
                                ]
                            },
                            {
                                "name": "肉類",
                                "options": [
                                    {
                                        "id": 3,
                                        "uuid": "43505359565a654a6758",
                                        "name": "恐龍肉",
                                        "sort": 1
                                    }
                                ]
                            }
                        ]
                    },
                    "photo": [
                        {
                            "url": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/4673bb9c-e2f7-4647-953c-76e849f059fc",
                            "sort": 1
                        }
                    ]
                },
                {
                    "id": 2,
                    "no": "2021080400001",
                    "name": "小米漢堡",
                    "price": "$100 ~ $10",
                    "intro": "商品簡介, 放在價格下面的文案",
                    "description": "<p>產品介紹，要可以放 HTML 內容</p>",
                    "recommended": "<p>推薦分享，要可以放 HTML 內容</p>",
                    "shippingMethods": [
                        {
                            "id": 1,
                            "name": "全家取貨付款"
                        }
                    ],
                    "paymentMethods": [
                        {
                            "id": 1,
                            "name": "全家店到店"
                        }
                    ],
                    "categories": [
                        {
                            "id": 1,
                            "name": "彩妝類",
                            "type": "big"
                        },
                        {
                            "id": 2,
                            "name": "彩妝用品",
                            "type": "medium"
                        },
                        {
                            "id": 3,
                            "name": "指甲彩繪",
                            "type": "small"
                        }
                    ],
                    "models": [
                        {
                            "id": 1,
                            "uuid": "5157424d597847674c64",
                            "uuids": [
                                "5157424d597847674c64"
                            ],
                            "name": "紅米",
                            "price": {
                                "original": 200,
                                "special": 100
                            },
                            "stockSurplus": 10
                        },
                        {
                            "id": 2,
                            "uuid": "5157424d597847674c65",
                            "uuids": [
                                "5157424d597847674c65"
                            ],
                            "name": "黑米",
                            "price": {
                                "original": 200,
                                "special": 100
                            },
                            "stockSurplus": 10
                        }
                    ],
                    "spec": {
                        "type": "single",
                        "list": [
                            {
                                "name": "外皮",
                                "options": [
                                    {
                                        "id": 1,
                                        "uuid": "5157424d597847674c64",
                                        "name": "紅米",
                                        "sort": 1
                                    },
                                    {
                                        "id": 2,
                                        "uuid": "5157424d597847674c65",
                                        "name": "黑米",
                                        "sort": 2
                                    }
                                ]
                            }
                        ]
                    },
                    "photo": [
                        {
                            "url": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/4673bb9c-e2f7-4647-953c-76e849f059fc",
                            "sort": 1
                        }
                    ]
                },
                {
                    "id": 3,
                    "no": "2021080400001",
                    "name": "小米漢堡",
                    "price": {
                        "original": 200,
                        "special": 100
                    },
                    "intro": "商品簡介, 放在價格下面的文案",
                    "description": "<p>產品介紹，要可以放 HTML 內容</p>",
                    "recommended": "<p>推薦分享，要可以放 HTML 內容</p>",
                    "shippingMethods": [
                        {
                            "id": 1,
                            "name": "全家取貨付款"
                        }
                    ],
                    "paymentMethods": [
                        {
                            "id": 1,
                            "name": "全家店到店"
                        }
                    ],
                    "categories": [
                        {
                            "id": 1,
                            "name": "彩妝類",
                            "type": "big"
                        },
                        {
                            "id": 2,
                            "name": "彩妝用品",
                            "type": "medium"
                        },
                        {
                            "id": 3,
                            "name": "指甲彩繪",
                            "type": "small"
                        }
                    ],
                    "models": [
                        {
                            "id": 1,
                            "uuid": "5157424d597847674c64",
                            "uuids": [
                                "5157424d597847674c64"
                            ],
                            "name": "無",
                            "price": {
                                "original": 200,
                                "special": 100
                            },
                            "stockSurplus": 10
                        }
                    ],
                    "spec": {
                        "type": "none",
                        "list": [
                            {
                                "name": "無",
                                "options": [
                                    {
                                        "id": 1,
                                        "uuid": "5157424d597847674c64",
                                        "name": "無",
                                        "sort": 0
                                    }
                                ]
                            }
                        ]
                    },
                    "photo": [
                        {
                            "url": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/4673bb9c-e2f7-4647-953c-76e849f059fc",
                            "sort": 1
                        }
                    ]
                }
            ],
            "links": {
                "first": "https://famiwhole.test/backend/supplier/products?page=1",
                "last": "https://famiwhole.test/backend/supplier/products?page=9",
                "prev": null,
                "next": "https://famiwhole.test/backend/supplier/products?page=2"
            },
            "meta": {
                "current_page": 1,
                "from": 1,
                "last_page": 9,
                "links": [
                    {
                        "url": null,
                        "label": "pagination.previous",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=1",
                        "label": "1",
                        "active": true
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=2",
                        "label": "2",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=3",
                        "label": "3",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=4",
                        "label": "4",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=5",
                        "label": "5",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=6",
                        "label": "6",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=7",
                        "label": "7",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=8",
                        "label": "8",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=9",
                        "label": "9",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=2",
                        "label": "pagination.next",
                        "active": false
                    }
                ],
                "path": "https://famiwhole.test/backend/supplier/products",
                "per_page": 5,
                "to": 5,
                "total": 45,
                "query": ""
            }
        }';
    }

    public function getBannerData()
    {
        return '{
            "data": [
                {
                    "href": "https://www.family.com.tw/Marketing/",
                    "src": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/staticblock_img1.jpg",
                    "blank": true
                },
                {
                    "href": "https://www.family.com.tw/Marketing/",
                    "src": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/staticblock_img2.jpg",
                    "blank": false
                },
                {
                    "href": "https://www.family.com.tw/Marketing/",
                    "src": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/staticblock_img3.jpg",
                    "blank": false
                }
            ]
        }';
    }
}
