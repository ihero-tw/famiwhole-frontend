<?php

namespace App\Http\Controllers\Web;

use App\Services\CartService;
use App\Services\ShopProductsService;
use App\Http\Controllers\Controller;
use App\Exceptions\RuntimeException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CartItemController extends Controller
{
    /**
     * ProductsService variable
     *
     * @var App\Services\CartService
     */
    protected $cartService;

    /**
     * __construct
     *
     */
    public function __construct(
        CartService $cartService,
        ShopProductsService $shopProductsService
    ) {
        $this->cartService = $cartService;
        $this->shopProductsService = $shopProductsService;
    }

    /**
     * Store item
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        try {
            $cartItem = $this->shopProductsService
                ->generateCartItem($request);

            $this->cartService
                ->putItem($cartItem, $request);

            return Redirect::back()->with('notify.success', '商品已加入購物車');
        } catch (RuntimeException $e) {
            return Redirect::back()->with('notify.error', $e->getMessage());
        }
    }

    /**
     * Update item data.
     *
     * @param Request $request
     * @return Response
     */
    public function update(Request $request, $shop, String $identity)
    {
        try {
            $this->cartService
                ->updateQty($identity, $request->input('qty'));

            return Redirect::back()->with('notify.success', '商品已更新');
        } catch (RuntimeException $e) {
            return Redirect::back()->with('notify.error', $e->getMessage());
        }
    }

    /**
     * Batch update data.
     *
     * @param Request $request
     * @return Response
     */
    public function destroy($shop, String $identity)
    {
        try {
            $this->cartService
                ->removeItem($identity);

            return Redirect::back();
        } catch (RuntimeException $e) {
            return Redirect::back()->with('notify.error', $e->getMessage());
        }
    }
}
