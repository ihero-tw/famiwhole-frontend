<?php

namespace App\Http\Controllers\Web;

use App\Http\Resources\MarketResource;
use App\Http\Resources\WebAccountRefundBankResource;
use App\Http\Controllers\Controller;
use Inertia\Inertia;

class AccountRefundBankController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index($shop)
    {
        return Inertia::render('Account/refund', [
            'refund' => new WebAccountRefundBankResource(new \stdClass())
        ]);
    }

    public function edit($shop)
    {
        return Inertia::render('Account/refundEdit', [
            'refund' => new WebAccountRefundBankResource(new \stdClass())
        ]);
    }

    public function update()
    {
        //
    }
}
