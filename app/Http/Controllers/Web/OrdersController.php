<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Exceptions\RuntimeException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Response;
use App\Http\Resources\MarketResource;

class OrdersController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index($shop)
    {
        return Inertia::render('Order/order', [
            'orders' => json_decode($this->getOrders())
        ]);
    }

    public function edit($shop)
    {
        return Inertia::render('Order/orderDetail', [
            'order' => json_decode($this->getOrder())
        ]);
    }

    public function cancel($shop)
    {
        try {
            return Redirect::back()->with('notify.success', '訂單已取消');
        } catch (RuntimeException $e) {
            return Redirect::back()->with('notify.error', $e->getMessage());
        }
    }

    public function getOrders()
    {
        return '
        {
            "data": [
                {
                    "no": "6000220339",
                    "photo": "https://famiwhole.test/goodsupply/shop/medias/35e07f9b-62f1-4c20-be50-ae86221aa1e5",
                    "amount": 30,
                    "total": "1000",
                    "createdAt": "2021-10-21 12:08:09"
                }
            ],
            "links": {
                "first": "https://famiwhole.test/goodsupply/88888806212099/orders?page=1",
                "last": "https://famiwhole.test/goodsupply/88888806212099/orders?page=1",
                "prev": null,
                "next": null
            },
            "meta": {
                "current_page": 1,
                "from": 1,
                "last_page": 1,
                "links": [
                    {
                        "url": null,
                        "label": "pagination.previous",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/goodsupply/88888806212099/orders?page=1",
                        "label": "1",
                        "active": true
                    },
                    {
                        "url": null,
                        "label": "pagination.next",
                        "active": false
                    }
                ],
                "path": "https://famiwhole.test/goodsupply/88888806212099/orders",
                "per_page": 5,
                "to": 1,
                "total": 1,
                "query": ""
            }
        }
        ';
    }

    public function getOrder()
    {
        return '
        {
            "data": {
                "info": {
                    "no": "6000220339",
                    "paymentMethod": "到店付款取貨",
                    "invoice": "會員載具, 09887766",
                    "total": "1000",
                    "createdAt": "2021-10-21 12:08:09"
                },
                "items": {
                    "1": {
                        "info": {
                            "supplier": "萬豪良品",
                            "notes": "快來哦",
                            "shippingMethod": "全家店到店-常溫",
                            "recipient": {
                                "name": "王小美",
                                "phone": "0988999888",
                                "address": {
                                    "city": "春日部",
                                    "town": "双葉町",
                                    "street": "雙葉町（双葉町）",
                                    "zipcode": "904"
                                }
                            },
                            "deliveryFee": 59,
                            "subtotal": 0,
                            "total": 10
                        },
                        "unshipped": [],
                        "shipments": [
                            {
                                "no": "S20987654321",
                                "number": "11023028075",
                                "stage": [
                                    {
                                        "name": "訂單成立",
                                        "date": "02/25 12:00",
                                        "status": "done"
                                    },
                                    {
                                        "name": "出貨中",
                                        "date": "02/26 12:00",
                                        "status": "done"
                                    },
                                    {
                                        "name": "已寄回",
                                        "date": "02/26 12:00",
                                        "status": "done"
                                    },
                                    {
                                        "name": "取件店關閉",
                                        "date": "02/26 12:00",
                                        "status": "last",
                                        "button": {
                                            "url": "",
                                            "label": "修改取件店舖"
                                        }
                                    },
                                    {
                                        "name": "貨到店舖",
                                        "date": "",
                                        "status": ""
                                    },
                                    {
                                        "name": "買家取貨",
                                        "date": "",
                                        "status": ""
                                    },
                                    {
                                        "name": "已完成",
                                        "date": "",
                                        "status": ""
                                    }
                                ],
                                "canReturn": true,
                                "appliedReturn": false,
                                "items": [
                                    {
                                        "id": 4,
                                        "name": "機械哥吉拉",
                                        "photo": "https://famiwhole.test/goodsupply/shop/medias/35e07f9b-62f1-4c20-be50-ae86221aa1e5",
                                        "spec": "L / 銀色",
                                        "price": "100",
                                        "amount": 10,
                                        "status": "訂單成立"
                                    },
                                    {
                                        "id": 5,
                                        "name": "機械哥吉拉",
                                        "photo": "https://famiwhole.test/goodsupply/shop/medias/35e07f9b-62f1-4c20-be50-ae86221aa1e5",
                                        "spec": "L / 黑色",
                                        "price": "200",
                                        "amount": 10,
                                        "status": "訂單成立"
                                    }
                                ]
                            }
                        ]
                    },
                    "9": {
                        "info": {
                            "supplier": "寶可夢有限公司",
                            "notes": null,
                            "shippingMethod": "全家店到店-常溫",
                            "recipient": {
                                "name": "王小美",
                                "phone": "0988999888",
                                "address": {
                                    "city": "春日部",
                                    "town": "双葉町",
                                    "street": "雙葉町（双葉町）",
                                    "zipcode": "904"
                                }
                            },
                            "deliveryFee": 48,
                            "subtotal": 0,
                            "total": 20
                        },
                        "unshipped": [
                            {
                                "id": 6,
                                "name": "冰茶1",
                                "photo": "https://famiwhole.test/goodsupply/shop/medias/35e07f9b-62f1-4c20-be50-ae86221aa1e5",
                                "spec": "L / 銀色",
                                "price": "100",
                                "amount": 10,
                                "status": "訂單成立"
                            },
                            {
                                "id": 8,
                                "name": "啤酒",
                                "photo": "https://famiwhole.test/goodsupply/shop/medias/35e07f9b-62f1-4c20-be50-ae86221aa1e5",
                                "spec": "無",
                                "price": "100",
                                "amount": 10,
                                "status": "訂單成立"
                            }
                        ],
                        "shipments": [
                            {
                                "no": "S20987654324",
                                "number": "11023028075",
                                "stage": [
                                    {
                                        "name": "訂單成立",
                                        "date": "02/25 12:00",
                                        "status": "done"
                                    },
                                    {
                                        "name": "出貨中",
                                        "date": "02/26 12:00",
                                        "status": "done"
                                    },
                                    {
                                        "name": "已寄回",
                                        "date": "02/26 12:00",
                                        "status": "done"
                                    },
                                    {
                                        "name": "取件店關閉",
                                        "date": "02/25 12:00",
                                        "status": "done",
                                        "message": {
                                            "color": "red",
                                            "text": "修改取件店:(019210)全家台中水景店"
                                        }
                                    },
                                    {
                                        "name": "貨到店舖",
                                        "date": "03/01 12:00",
                                        "status": "done"
                                    },
                                    {
                                        "name": "買家取貨",
                                        "date": "02/25 12:00",
                                        "status": "done"
                                    },
                                    {
                                        "name": "已完成",
                                        "date": "02/25 12:00",
                                        "status": "last"
                                    }
                                ],
                                "canReturn": false,
                                "appliedReturn": false,
                                "items": [
                                    {
                                        "id": 10,
                                        "name": "啤酒",
                                        "photo": "https://famiwhole.test/goodsupply/shop/medias/35e07f9b-62f1-4c20-be50-ae86221aa1e5",
                                        "spec": "無",
                                        "price": "100",
                                        "amount": 10,
                                        "status": "訂單成立"
                                    }
                                ]
                            }
                        ]
                    }
                }
            }
        }
        ';
    }
}
