<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProductController extends Controller
{
    /**
     * __construct.
     */
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the accounts.
     *
     * @return Response
     */
    public function index($shop, $id = '')
    {
        // dd(request()->route()->parameters);
        return Inertia::render('Product/Index', [
            'id' => $id,
            'market' => json_decode($this->getMarketData()),
            'product' => json_decode($this->getDataByDoubleSpec()),
            'related' => json_decode($this->getRelatedProductData())
        ]);
    }

    public function search($shop, $text)
    {
        return Inertia::render('Product/Search', [
            'search'  => $text,
            'market' => json_decode($this->getMarketData()),
            'products' => json_decode($this->getRelatedProductData()),
        ]);
    }

    public function getDataByDoubleSpec()
    {
        return '{
            "data": {
                "id": 1,
                "no": "2021080400001",
                "name": "小米漢堡",
                "price": "$100 ~ $10",
                "intro": "商品簡介, 放在價格下面的文案",
                "description": "<p>產品介紹，要可以放 HTML 內容</p>",
                "recommended": "<p>推薦分享，要可以放 HTML 內容</p>",
                "shippingMethods": [
                    {
                        "id": 1,
                        "name": "全家取貨付款"
                    }
                ],
                "paymentMethods": [
                    {
                        "id": 1,
                        "name": "全家店到店"
                    }
                ],
                "categories": [
                    {
                        "id": 1,
                        "name": "彩妝類",
                        "type": "big"
                    },
                    {
                        "id": 2,
                        "name": "彩妝用品",
                        "type": "medium"
                    },
                    {
                        "id": 3,
                        "name": "指甲彩繪",
                        "type": "small"
                    }
                ],
                "models": [
                    {
                        "id": 1,
                        "uuid": "5157424d597847674c64-43505359565a654a6758",
                        "uuids": [
                            "5157424d597847674c64",
                            "43505359565a654a6758"
                        ],
                        "name": "紅 / M",
                        "sku": "GB001",
                        "price": {
                            "original": 200,
                            "special": 100
                        },
                        "stockSurplus": 20
                    },
                    {
                        "id": 2,
                        "uuid": "5157424d597847674c65-43505359565a654a6758",
                        "uuids": [
                            "5157424d597847674c65",
                            "43505359565a654a6758"
                        ],
                        "name": "黑 / M",
                        "sku": "GB002",
                        "price": {
                            "original": 200,
                            "special": 10
                        },
                        "stockSurplus": 10
                    },
                    {
                        "id": 2,
                        "uuid": "5157424d597847674c64-435053595d8j1931l221",
                        "uuids": [
                            "5157424d597847674c64",
                            "435053595d8j1931l221"
                        ],
                        "name": "紅 / XL",
                        "sku": "GB002",
                        "price": {
                            "original": 200,
                            "special": 10
                        },
                        "stockSurplus": 0
                    },
                    {
                        "id": 2,
                        "uuid": "5157424d597847674c65-435053595d8j1931l221",
                        "uuids": [
                            "5157424d597847674c65",
                            "435053595d8j1931l221"
                        ],
                        "name": "黑 / XL",
                        "sku": "GB002",
                        "price": {
                            "original": 200,
                            "special": 10
                        },
                        "stockSurplus": 50
                    }
                ],
                "spec": {
                    "type": "double",
                    "list": [
                        {
                            "name": "款式",
                            "options": [
                                {
                                    "id": 1,
                                    "uuid": "5157424d597847674c64",
                                    "name": "紅",
                                    "sort": 1
                                },
                                {
                                    "id": 2,
                                    "uuid": "5157424d597847674c65",
                                    "name": "黑",
                                    "sort": 2
                                }
                            ]
                        },
                        {
                            "name": "尺寸",
                            "options": [
                                {
                                    "id": 3,
                                    "uuid": "43505359565a654a6758",
                                    "name": "M",
                                    "sort": 1
                                }, {
                                    "id": 4,
                                    "uuid": "435053595d8j1931l221",
                                    "name": "XL",
                                    "sort": 1
                                }
                            ]
                        }
                    ]
                },
                "photo": [
                    {
                        "url": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/4673bb9c-e2f7-4647-953c-76e849f059fc",
                        "sort": 1
                    }
                ]
            }
        }';
    }

    public function getDataBySingleSpec()
    {
        return '{
            "data": {
                "id": 1,
                "no": "2021080400001",
                "name": "小米漢堡",
                "price": "$100 ~ $10",
                "intro": "商品簡介, 放在價格下面的文案",
                "description": "<p>產品介紹，要可以放 HTML 內容</p>",
                "recommended": "<p>推薦分享，要可以放 HTML 內容</p>",
                "shippingMethods": [
                    {
                        "id": 1,
                        "name": "全家取貨付款"
                    }
                ],
                "paymentMethods": [
                    {
                        "id": 1,
                        "name": "全家店到店"
                    }
                ],
                "categories": [
                    {
                        "id": 1,
                        "name": "彩妝類",
                        "type": "big"
                    },
                    {
                        "id": 2,
                        "name": "彩妝用品",
                        "type": "medium"
                    },
                    {
                        "id": 3,
                        "name": "指甲彩繪",
                        "type": "small"
                    }
                ],
                "models": [
                    {
                        "id": 1,
                        "uuid": "5157424d597847674c64",
                        "uuids": [
                            "5157424d597847674c64"
                        ],
                        "name": "紅米",
                        "price": {
                            "original": 200,
                            "special": 100
                        },
                        "stockSurplus": 10
                    },
                    {
                        "id": 2,
                        "uuid": "5157424d597847674c65",
                        "uuids": [
                            "5157424d597847674c65"
                        ],
                        "name": "黑米",
                        "price": {
                            "original": 200,
                            "special": 100
                        },
                        "stockSurplus": 10
                    }
                ],
                "spec": {
                    "type": "single",
                    "list": [
                        {
                            "name": "外皮",
                            "options": [
                                {
                                    "id": 1,
                                    "uuid": "5157424d597847674c64",
                                    "name": "紅米",
                                    "sort": 1
                                },
                                {
                                    "id": 2,
                                    "uuid": "5157424d597847674c65",
                                    "name": "黑米",
                                    "sort": 2
                                }
                            ]
                        }
                    ]
                },
                "photo": [
                    {
                        "url": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/4673bb9c-e2f7-4647-953c-76e849f059fc",
                        "sort": 1
                    }
                ]
            }
        }';
    }

    public function getDataByNoneSpec()
    {
        return '{
            "data": {
                "id": 1,
                "no": "2021080400001",
                "name": "小米漢堡",
                "price": {
                    "original": 200,
                    "special": 100
                },
                "intro": "商品簡介, 放在價格下面的文案",
                "description": "<p>產品介紹，要可以放 HTML 內容</p>",
                "recommended": "<p>推薦分享，要可以放 HTML 內容</p>",
                "shippingMethods": [
                    {
                        "id": 1,
                        "name": "全家取貨付款"
                    }
                ],
                "paymentMethods": [
                    {
                        "id": 1,
                        "name": "全家店到店"
                    }
                ],
                "categories": [
                    {
                        "id": 1,
                        "name": "彩妝類",
                        "type": "big"
                    },
                    {
                        "id": 2,
                        "name": "彩妝用品",
                        "type": "medium"
                    },
                    {
                        "id": 3,
                        "name": "指甲彩繪",
                        "type": "small"
                    }
                ],
                "models": [
                    {
                        "id": 1,
                        "uuid": "5157424d597847674c64",
                        "uuids": [
                            "5157424d597847674c64"
                        ],
                        "name": "無",
                        "price": {
                            "original": 200,
                            "special": 100
                        },
                        "stockSurplus": 10
                    }
                ],
                "spec": {
                    "type": "none",
                    "list": [
                        {
                            "name": "無",
                            "options": [
                                {
                                    "id": 1,
                                    "uuid": "5157424d597847674c64",
                                    "name": "無",
                                    "sort": 0
                                }
                            ]
                        }
                    ]
                },
                "photo": [
                    {
                        "url": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/4673bb9c-e2f7-4647-953c-76e849f059fc",
                        "sort": 1
                    }
                ]
            }
        }';
    }

    public function getRelatedProductData()
    {
        return '{
            "data": [
                {
                    "id": 112,
                    "no": "P09115027",
                    "name": "測試",
                    "intro": "測試",
                    "description": null,
                    "photo": [
                        {
                            "id": "6423cb9c-3d34-4dea-aa86-36f3f349a018",
                            "sort": 1
                        }
                    ],
                    "spec": {
                        "type": "none",
                        "list": [
                            {
                                "id": 114,
                                "name": "無",
                                "options": [
                                    {
                                        "id": 396,
                                        "uuid": "52354346763646434c36",
                                        "name": "無",
                                        "sort": 1
                                    }
                                ]
                            }
                        ]
                    },
                    "models": [
                        {
                            "id": 185,
                            "uuids": [
                                "52354346763646434c36"
                            ],
                            "names": [
                                "無"
                            ],
                            "sku": "OK009",
                            "price": {
                                "original": "220",
                                "special": "100"
                            },
                            "stock": {
                                "setup": 100,
                                "surplus": 0,
                                "safety": 0
                            }
                        }
                    ],
                    "keywords": [
                        ""
                    ],
                    "shippingMethods": [
                        {
                            "id": 1,
                            "name": "全家取貨付款"
                        }
                    ],
                    "categories": [
                        {
                            "id": 1,
                            "name": "彩妝類",
                            "type": "big"
                        },
                        {
                            "id": 2,
                            "name": "彩妝用品",
                            "type": "medium"
                        },
                        {
                            "id": 3,
                            "name": "指甲彩繪",
                            "type": "small"
                        }
                    ]
                },
                {
                    "id": 111,
                    "no": "P09115027",
                    "name": "測試",
                    "intro": "測試",
                    "description": null,
                    "profitSharingId": 1,
                    "photo": [
                        {
                            "id": "9b124f56-8501-4d6f-bf28-72944b383190",
                            "sort": 1
                        }
                    ],
                    "spec": {
                        "type": "none",
                        "list": [
                            {
                                "id": 113,
                                "name": "無",
                                "options": [
                                    {
                                        "id": 395,
                                        "uuid": "5157336f76465962684d",
                                        "name": "無",
                                        "sort": 1
                                    }
                                ]
                            }
                        ]
                    },
                    "models": [
                        {
                            "id": 184,
                            "uuids": [
                                "5157336f76465962684d"
                            ],
                            "names": [
                                "無"
                            ],
                            "sku": "OK009",
                            "price": {
                                "original": "220",
                                "special": "100"
                            },
                            "stock": {
                                "setup": 100,
                                "surplus": 0,
                                "safety": 0
                            }
                        }
                    ],
                    "keywords": [
                        ""
                    ],
                    "shippingMethods": [
                        {
                            "id": 1,
                            "name": "全家取貨付款"
                        }
                    ],
                    "paymentMethods": [
                        {
                            "id": 1,
                            "name": "全家店到店"
                        }
                    ],
                    "categories": [
                        {
                            "id": 1,
                            "name": "彩妝類",
                            "type": "big"
                        },
                        {
                            "id": 2,
                            "name": "彩妝用品",
                            "type": "medium"
                        },
                        {
                            "id": 3,
                            "name": "指甲彩繪",
                            "type": "small"
                        }
                    ]
                },
                {
                    "id": 110,
                    "no": "IMP001",
                    "name": "匯入測試",
                    "intro": "這是測試的",
                    "description": "這是測試的",
                    "profitSharingId": 1,
                    "photo": [
                        {
                            "id": "0c60132f-52d1-4856-9b20-4120a2255a5b",
                            "sort": 1
                        },
                        {
                            "id": "5aef32e9-d1ff-4c4e-9aa2-7835658f7f73",
                            "sort": 1
                        },
                        {
                            "id": "5b77b8e4-278c-4b5c-82e4-096065b5a729",
                            "sort": 1
                        },
                        {
                            "id": "65fdd2ab-ad31-438a-9f58-a4e0f8501e1c",
                            "sort": 1
                        },
                        {
                            "id": "7458fc75-ded6-4983-be83-91b8e3842e0a",
                            "sort": 1
                        },
                        {
                            "id": "abe69c32-5e06-4ef6-bfc4-65d997136e31",
                            "sort": 1
                        },
                        {
                            "id": "b37f02b0-2611-4d32-a275-ac3e348718a7",
                            "sort": 1
                        },
                        {
                            "id": "b45dadb2-7bd5-4ed3-af94-232358922db4",
                            "sort": 1
                        },
                        {
                            "id": "f848db0e-21c1-4d04-b2ca-354f41b2edc4",
                            "sort": 1
                        }
                    ],
                    "spec": {
                        "type": "double",
                        "list": [
                            {
                                "id": 111,
                                "name": "顏色",
                                "options": [
                                    {
                                        "id": 391,
                                        "uuid": "34357a534b6771523670",
                                        "name": "紅",
                                        "sort": 0
                                    },
                                    {
                                        "id": 392,
                                        "uuid": "5042626a467531376c62",
                                        "name": "黑",
                                        "sort": 999
                                    }
                                ]
                            },
                            {
                                "id": 112,
                                "name": "尺寸",
                                "options": [
                                    {
                                        "id": 393,
                                        "uuid": "615a496158305a59534e",
                                        "name": "L",
                                        "sort": 0
                                    },
                                    {
                                        "id": 394,
                                        "uuid": "43355579616d64366e45",
                                        "name": "M",
                                        "sort": 999
                                    }
                                ]
                            }
                        ]
                    },
                    "models": [
                        {
                            "id": 181,
                            "uuids": [
                                "5042626a467531376c62",
                                "615a496158305a59534e"
                            ],
                            "names": [
                                "黑",
                                "L"
                            ],
                            "sku": "GB001",
                            "price": {
                                "original": "200",
                                "special": "100"
                            },
                            "stock": {
                                "setup": 100,
                                "surplus": 0,
                                "safety": 0
                            }
                        },
                        {
                            "id": 182,
                            "uuids": [
                                "34357a534b6771523670",
                                "43355579616d64366e45"
                            ],
                            "names": [
                                "紅",
                                "M"
                            ],
                            "sku": null,
                            "price": {
                                "original": "20",
                                "special": "0"
                            },
                            "stock": {
                                "setup": 0,
                                "surplus": 0,
                                "safety": 0
                            }
                        },
                        {
                            "id": 183,
                            "uuids": [
                                "5042626a467531376c62",
                                "43355579616d64366e45"
                            ],
                            "names": [
                                "黑",
                                "M"
                            ],
                            "sku": "GB002",
                            "price": {
                                "original": "200",
                                "special": "100"
                            },
                            "stock": {
                                "setup": 100,
                                "surplus": 0,
                                "safety": 0
                            }
                        }
                    ],
                    "keywords": [
                        ""
                    ],
                    "shippingMethods": [
                        {
                            "id": 1,
                            "name": "全家取貨付款"
                        }
                    ],
                    "paymentMethods": [
                        {
                            "id": 1,
                            "name": "全家店到店"
                        }
                    ],
                    "categories": [
                        {
                            "id": 1,
                            "name": "彩妝類",
                            "type": "big"
                        },
                        {
                            "id": 2,
                            "name": "彩妝用品",
                            "type": "medium"
                        },
                        {
                            "id": 3,
                            "name": "指甲彩繪",
                            "type": "small"
                        }
                    ]
                },
                {
                    "id": 109,
                    "no": "P09115027",
                    "name": "測試",
                    "intro": "測試",
                    "description": null,
                    "photo": [
                        {
                            "id": "c710ed27-5b79-434d-b6c2-824ff5b29eb3",
                            "sort": 1
                        }
                    ],
                    "spec": {
                        "type": "none",
                        "list": [
                            {
                                "id": 110,
                                "name": "無",
                                "options": [
                                    {
                                        "id": 390,
                                        "uuid": "53456e6f48387a33466b",
                                        "name": "無",
                                        "sort": 1
                                    }
                                ]
                            }
                        ]
                    },
                    "models": [
                        {
                            "id": 180,
                            "uuids": [
                                "53456e6f48387a33466b"
                            ],
                            "names": [
                                "無"
                            ],
                            "sku": "OK009",
                            "price": {
                                "original": "220",
                                "special": "100"
                            },
                            "stock": {
                                "setup": 100,
                                "surplus": 0,
                                "safety": 0
                            }
                        }
                    ],
                    "keywords": [
                        ""
                    ],
                    "shippingMethods": [
                        {
                            "id": 1,
                            "name": "全家取貨付款"
                        }
                    ],
                    "paymentMethods": [
                        {
                            "id": 1,
                            "name": "全家店到店"
                        }
                    ],
                    "categories": [
                        {
                            "id": 1,
                            "name": "彩妝類",
                            "type": "big"
                        },
                        {
                            "id": 2,
                            "name": "彩妝用品",
                            "type": "medium"
                        },
                        {
                            "id": 3,
                            "name": "指甲彩繪",
                            "type": "small"
                        }
                    ]
                },
                {
                    "id": 108,
                    "no": "P09115027",
                    "name": "測試",
                    "intro": "測試",
                    "description": null,
                    "profitSharingId": 1,
                    "photo": [],
                    "spec": {
                        "type": "none",
                        "list": [
                            {
                                "id": 109,
                                "name": "無",
                                "options": [
                                    {
                                        "id": 389,
                                        "uuid": "73566d37585373506f4d",
                                        "name": "無",
                                        "sort": 1
                                    }
                                ]
                            }
                        ]
                    },
                    "models": [
                        {
                            "id": 179,
                            "uuids": [
                                "73566d37585373506f4d"
                            ],
                            "names": [
                                "無"
                            ],
                            "sku": "OK009",
                            "price": {
                                "original": "220",
                                "special": "100"
                            },
                            "stock": {
                                "setup": 100,
                                "surplus": 0,
                                "safety": 0
                            }
                        }
                    ],
                    "keywords": [
                        ""
                    ],
                    "shippingMethods": [
                        {
                            "id": 1,
                            "name": "全家取貨付款"
                        }
                    ],
                    "paymentMethods": [
                        {
                            "id": 1,
                            "name": "全家店到店"
                        }
                    ],
                    "categories": [
                        {
                            "id": 1,
                            "name": "彩妝類",
                            "type": "big"
                        },
                        {
                            "id": 2,
                            "name": "彩妝用品",
                            "type": "medium"
                        },
                        {
                            "id": 3,
                            "name": "指甲彩繪",
                            "type": "small"
                        }
                    ]
                }
            ],
            "links": {
                "first": "https://famiwhole.test/backend/supplier/products?page=1",
                "last": "https://famiwhole.test/backend/supplier/products?page=9",
                "prev": null,
                "next": "https://famiwhole.test/backend/supplier/products?page=2"
            },
            "meta": {
                "current_page": 1,
                "from": 1,
                "last_page": 9,
                "links": [
                    {
                        "url": null,
                        "label": "pagination.previous",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=1",
                        "label": "1",
                        "active": true
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=2",
                        "label": "2",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=3",
                        "label": "3",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=4",
                        "label": "4",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=5",
                        "label": "5",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=6",
                        "label": "6",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=7",
                        "label": "7",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=8",
                        "label": "8",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=9",
                        "label": "9",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/backend/supplier/products?page=2",
                        "label": "pagination.next",
                        "active": false
                    }
                ],
                "path": "https://famiwhole.test/backend/supplier/products",
                "per_page": 5,
                "to": 5,
                "total": 45,
                "query": ""
            }
        }';
    }

    public function getMarketData()
    {
        return '{
            "data": {
                "no" : "5157424d",
                "name" : "所有商品",
                "intro" : "<p>這裡是可以放 html 的文案內容。<img src=images/inner-banner.jpg></p>",
                "list" : [
                    {
                        "no" : "5157424d",
                        "name" : "所有商品",
                        "url" : "/goodsupply/999999/market/5157424d/products"
                    },
                    {
                        "no" : "59784767",
                        "name" : "蘋果熱銷品推薦",
                        "url" : "/goodsupply/999999/market/59784767/products"
                    },
                    {
                        "no" : "4c641da7",
                        "name" : "新品上架",
                        "url" : "/goodsupply/999999/market/4c641da7/products"
                    },
                    {
                        "no" : "1d39s004",
                        "name" : "iPhone保護貼",
                        "url" : "/goodsupply/999999/market/1d39s004/products"
                    },
                    {
                        "no" : "4s30058",
                        "name" : "iPhone保護殼",
                        "url" : "/goodsupply/999999/market/4s30058/products"
                    },
                    {
                        "no" : "984ad31",
                        "name" : "iPad保護貼/保護殼/平板配件",
                        "url" : "/goodsupply/999999/market/984ad31/products"
                    },
                    {
                        "no" : "8471kae3",
                        "name" : "AirPods配件",
                        "url" : "/goodsupply/999999/market/8471kae3/products"
                    },
                    {
                        "no" : "85710i31",
                        "name" : "Apple Watch配件",
                        "url" : "/goodsupply/999999/market/85710i31/products"
                    }
                ]
            }
        }';
    }
}
