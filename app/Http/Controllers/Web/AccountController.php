<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Resources\WebAccountResource;
use App\Http\Resources\MarketResource;
use Inertia\Inertia;

class AccountController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index($shop)
    {
        return Inertia::render('Account/account', [
            'account' => new WebAccountResource(new \stdClass())
        ]);
    }

    public function update()
    {
        # code...
    }
}
