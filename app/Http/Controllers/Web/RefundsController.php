<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Exceptions\RuntimeException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Response;
use App\Http\Resources\MarketResource;

class RefundsController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index($shop)
    {
        return Inertia::render('Refund/refund', [
            'refunds' => json_decode($this->getRefunds()),
        ]);
    }

    public function edit($shop)
    {
        return Inertia::render('Refund/refundDetail', [
            'refund' => json_decode($this->getRefund()),
        ]);
    }

    public function store(Request $request)
    {
        try {
            return Redirect::back()->with('notify.success', '退貨申請已送出');
        } catch (RuntimeException $e) {
            return Redirect::back()->with('notify.error', $e->getMessage());
        }
    }

    public function getRefunds()
    {
        return '
        {
            "data": [
                {
                    "no": "S20987654324",
                    "photo": "https://famiwhole.test/goodsupply/shop/medias/35e07f9b-62f1-4c20-be50-ae86221aa1e5",
                    "amount": 1,
                    "total": "900",
                    "createdAt": "2021-11-03 02:12:26",
                    "status": "退貨申請"
                }
            ],
            "links": {
                "first": "https://famiwhole.test/goodsupply/88888806212099/refunds?page=1",
                "last": "https://famiwhole.test/goodsupply/88888806212099/refunds?page=1",
                "prev": null,
                "next": null
            },
            "meta": {
                "current_page": 1,
                "from": 1,
                "last_page": 1,
                "links": [
                    {
                        "url": null,
                        "label": "pagination.previous",
                        "active": false
                    },
                    {
                        "url": "https://famiwhole.test/goodsupply/88888806212099/refunds?page=1",
                        "label": "1",
                        "active": true
                    },
                    {
                        "url": null,
                        "label": "pagination.next",
                        "active": false
                    }
                ],
                "path": "https://famiwhole.test/goodsupply/88888806212099/refunds",
                "per_page": 5,
                "to": 1,
                "total": 1,
                "query": ""
            }
        }
        ';
    }

    public function getRefund()
    {
        return '
        {
            "data": {
                "no": "S20987654324",
                "number": "11023028075",
                "status": "退貨申請",
                "orderUrl": "/goodsupply/88888806212099/orders/6000220339",
                "createdAt": "2021-11-03 02:12:26",
                "stage": [
                    {
                        "name": "訂單成立",
                        "date": "02/25 12:00",
                        "status": "done"
                    },
                    {
                        "name": "出貨中",
                        "date": "02/26 12:00",
                        "status": "done"
                    },
                    {
                        "name": "已寄回",
                        "date": "02/26 12:00",
                        "status": "done"
                    },
                    {
                        "name": "取件店關閉",
                        "date": "02/26 12:00",
                        "status": "last",
                        "button": {
                            "url": "",
                            "label": "修改取件店舖"
                        }
                    },
                    {
                        "name": "貨到店舖",
                        "date": "",
                        "status": ""
                    },
                    {
                        "name": "買家取貨",
                        "date": "",
                        "status": ""
                    },
                    {
                        "name": "已完成",
                        "date": "",
                        "status": ""
                    }
                ],
                "stageExamine": "pending",
                "refund": {
                    "no": "202107010001",
                    "status": "尚未退款",
                    "bank": {
                        "code": "115",
                        "name": "基隆市第二信用合作社",
                        "branch": {
                            "code": "0096",
                            "name": "暖暖分社"
                        }
                    },
                    "account": {
                        "number": "01641205207812",
                        "name": "王小雪"
                    },
                    "identity": {
                        "type": "personal",
                        "ext": "83462647"
                    },
                    "completeAt": ""
                },
                "items": [
                    {
                        "id": 1,
                        "name": "機械哥吉拉",
                        "photo": "https://famiwhole.test/goodsupply/shop/medias/35e07f9b-62f1-4c20-be50-ae86221aa1e5",
                        "spec": "L / 銀色",
                        "price": "0",
                        "amount": 3,
                        "returned": {
                            "causer": {
                                "reason": "尺寸版型不合/顏色不喜歡",
                                "note": "寄錯了"
                            },
                            "photos": [
                                "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/35e07f9b-62f1-4c20-be50-ae86221aa1e5",
                                "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/0fa5d917-898e-4ac3-aaf9-1c6fba81a499",
                                "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/b7bd76b8-20bf-4163-a815-0308c2a67cd8"
                            ]
                        },
                        "examined": {
                            "amount": {
                                "successful": 2,
                                "failure": 1
                            },
                            "explain": "只有收到2雙",
                            "photos": [
                                "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/35e07f9b-62f1-4c20-be50-ae86221aa1e5",
                                "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/0fa5d917-898e-4ac3-aaf9-1c6fba81a499",
                                "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/b7bd76b8-20bf-4163-a815-0308c2a67cd8"
                            ]
                        },
                        "agreed": {
                            "amount": {
                                "successful": 3,
                                "failure": 0
                            },
                            "explain": "我有寄回去哦"
                        },
                        "adjudication": {
                            "amount": {
                                "successful": 2,
                                "failure": 1
                            },
                            "explain": "賣家沒有寄回去"
                        }
                    }
                ]
            }
        }
        ';
    }
}
