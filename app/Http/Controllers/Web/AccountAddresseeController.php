<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Resources\WebAccountAddresseeResource;
use Inertia\Inertia;

use App\Http\Resources\MarketResource;

class AccountAddresseeController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index($shop)
    {
        return Inertia::render('Account/addresse', [
            'addressees' => WebAccountAddresseeResource::collection($this->getAddressees()),
        ]);
    }

    public function getAddressees()
    {
        return  collect([
            [
                'id' => 1,
                'type' => 'home',
                'default' => true,
                'name' => '陳小美',
                'phone' => '0911123456',
                'address' => '407臺中市西屯區市政路500號24樓之8',
            ],
            [
                'id' => 2,
                'type' => 'home',
                'default' => false,
                'name' => '陳小美',
                'phone' => '0911123456',
                'address' => '200臺北市路500號24樓之8',
            ],
            [
                'id' => 3,
                'type' => 'market',
                'default' => true,
                'name' => '陳小美',
                'phone' => '0911123456',
                'address' => '(018015)全家台中和順店',
            ],
            [
                'id' => 4,
                'type' => 'market',
                'default' => true,
                'name' => '陳小美',
                'phone' => '0911123456',
                'address' => '(019000)全家台中萬寶店',
            ],
            [
                'id' => 5,
                'type' => 'market',
                'default' => true,
                'name' => '陳小美',
                'phone' => '0911123456',
                'address' => '(087778)全家台中通通店',
            ],
        ])->map(function ($item) {
            $address = new \stdClass();
            $address->id = $item['id'];
            $address->type = $item['type'];
            $address->default = $item['default'];
            $address->name = $item['name'];
            $address->phone = $item['phone'];
            $address->address = $item['address'];
            return $address;
        });
    }
}
