<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Exceptions\RuntimeException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Response;
use App\Http\Resources\MarketResource;
class ShoppingController extends Controller
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Display listing of cart.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function cart($shop)
    {
        return Inertia::render('Shopping/Cart', [
            'shoppingListing' => json_decode($this->getShoppingCart()),
            'user' => json_decode($this->getUser())
        ]);
    }

    public function generateCheckout(Request $request)
    {
        try {
            return Redirect::route('web.shopping.checkout.show', [$request->shop->no, '4565594b77713436514f']);
        } catch (RuntimeException $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function showCheckout($shop)
    {
        return Inertia::render('Shopping/Checkout', [
            'checkout' => json_decode($this->getCheckout()),
            'user' => json_decode($this->getUser())
        ]);
    }

    public function done($shop)
    {
        return Inertia::render('Shopping/Done', [
        ]);
    }

    public function getShoppingCart()
    {
        return '
        {
            "data": [
                {
                    "supplier": "萬豪良品",
                    "count": 3,
                    "items": [
                        {
                            "identity": "73686f7070696e672d3136",
                            "qty": 2,
                            "product": {
                                "name": "青島社 - 三式機龍 組合模型",
                                "photo": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/867fe0e9-c68e-48ea-b6b4-0f44dc7cee4f",
                                "shippingMethods": [
                                    "全家店到店-常溫"
                                ]
                            },
                            "model": {
                                "name": "AG001",
                                "price": {
                                    "original": "4500",
                                    "special": "2999"
                                },
                                "stockSurplus": 100
                            }
                        },
                        {
                            "identity": "73686f7070696e672d3139",
                            "qty": 0,
                            "product": {
                                "name": "SHM 傳說哥吉拉",
                                "photo": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/278e43e5-8b99-4e9c-a393-7cc19763610d",
                                "shippingMethods": [
                                    "全家店到店-常溫"
                                ]
                            },
                            "model": {
                                "name": "無",
                                "price": {
                                    "original": "10000",
                                    "special": "1300"
                                },
                                "stockSurplus": 1000
                            }
                        },
                        {
                            "identity": "73686f7070696e672d32",
                            "qty": 5,
                            "product": {
                                "name": "機械哥吉拉",
                                "photo": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/197a3e46-e81e-4d1a-b75f-ef95e06512f9",
                                "shippingMethods": [
                                    "全家店到店-常溫",
                                    "全家店到店-冷凍"
                                ]
                            },
                            "model": {
                                "name": "L / 白色",
                                "price": {
                                    "original": "1000",
                                    "special": "800"
                                },
                                "stockSurplus": 40
                            }
                        }
                    ]
                },
                {
                    "supplier": "貓王國",
                    "count": 1,
                    "items": [
                        {
                            "identity": "73686f7070696e672d3232",
                            "qty": 0,
                            "product": {
                                "name": "BoosCAT",
                                "photo": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/43318ab8-c458-46af-b52d-609c38004a03",
                                "shippingMethods": [
                                    "全家店到店-常溫",
                                    "全家店到店-冷凍"
                                ]
                            },
                            "model": {
                                "name": "無",
                                "price": {
                                    "original": "1000",
                                    "special": "990"
                                },
                                "stockSurplus": 1000
                            }
                        }
                    ]
                }
            ]
        }';
    }

    public function getCheckout()
    {
        return '
        {
            "uuid": "4565594b77713436514f",
            "paymentMehods": {
                "data": [
                    {
                        "id": 1,
                        "title": "到店付款取貨",
                        "notice": "若結帳總金額超過2萬元無法選擇此付款方式"
                    }
                ]
            },
            "items": {
                "data": [
                    {
                        "identity": "737570706c6965722d31",
                        "supplier": "萬豪良品",
                        "count": 3,
                        "items": [
                            {
                                "identity": "73686f7070696e672d3136",
                                "qty": 2,
                                "subtotal": 5998,
                                "product": {
                                    "name": "青島社 - 三式機龍 組合模型",
                                    "photo": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/867fe0e9-c68e-48ea-b6b4-0f44dc7cee4f"
                                },
                                "model": {
                                    "name": "AG001",
                                    "price": {
                                        "original": "4500",
                                        "special": "2999"
                                    }
                                }
                            },
                            {
                                "identity": "73686f7070696e672d3139",
                                "qty": 0,
                                "subtotal": 0,
                                "product": {
                                    "name": "SHM 傳說哥吉拉",
                                    "photo": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/278e43e5-8b99-4e9c-a393-7cc19763610d"
                                },
                                "model": {
                                    "name": "無",
                                    "price": {
                                        "original": "10000",
                                        "special": "1300"
                                    }
                                }
                            },
                            {
                                "identity": "73686f7070696e672d32",
                                "qty": 5,
                                "subtotal": 4000,
                                "product": {
                                    "name": "機械哥吉拉",
                                    "photo": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/197a3e46-e81e-4d1a-b75f-ef95e06512f9"
                                },
                                "model": {
                                    "name": "L / 白色",
                                    "price": {
                                        "original": "1000",
                                        "special": "800"
                                    }
                                }
                            }
                        ],
                        "payable": {
                            "subtotal": 9998,
                            "fee": 0,
                            "total": 9998
                        },
                        "shippingMethods": [
                            {
                                "id": 1,
                                "title": "全家店到店-常溫",
                                "type": "market",
                                "notice": "未滿 NT$1,000 運費 NT$80"
                            }
                        ]
                    },
                    {
                        "identity": "737570706c6965722d3139",
                        "supplier": "貓王國",
                        "count": 1,
                        "items": [
                            {
                                "identity": "73686f7070696e672d3232",
                                "qty": 0,
                                "subtotal": 0,
                                "product": {
                                    "name": "BoosCAT",
                                    "photo": "https://storagegoodsupplytest.blob.core.windows.net/goodsupplytest-pic-dev/medias/43318ab8-c458-46af-b52d-609c38004a03"
                                },
                                "model": {
                                    "name": "無",
                                    "price": {
                                        "original": "1000",
                                        "special": "990"
                                    }
                                }
                            }
                        ],
                        "payable": {
                            "subtotal": 0,
                            "fee": 0,
                            "total": 0
                        },
                        "shippingMethods": [
                            {
                                "id": 1,
                                "title": "全家店到店-常溫",
                                "type": "market",
                                "notice": "未滿 NT$1,000 運費 NT$80"
                            },
                            {
                                "id": 2,
                                "title": "全家店到店-冷凍",
                                "type": "market",
                                "notice": "未滿 NT$1,000 運費 NT$80"
                            }
                        ]
                    }
                ]
            },
            "total": 9998,
            "processing": null
        }';
    }

    public function getUser()
    {
        return '{
            "id": 1,
            "name": "AJieee",
            "sex": "male",
            "email": "ajieee@gmail.com",
            "phone": "0988999999",
            "notification": {
                "list": {
                    "data": []
                },
                "unread": 0
            },
            "addressees": {
                "data": [
                    {
                        "id": 1,
                        "type": "home",
                        "default": true,
                        "name": "陳小美",
                        "phone": "0911123456",
                        "address": "407臺中市西屯區市政路500號24樓之8"
                    },
                    {
                        "id": 2,
                        "type": "home",
                        "default": false,
                        "name": "陳小美",
                        "phone": "0911123456",
                        "address": "200臺北市路500號24樓之8"
                    },
                    {
                        "id": 3,
                        "type": "market",
                        "default": false,
                        "name": "陳小美",
                        "phone": "0911123456",
                        "address": "(018015)全家台中和順店"
                    },
                    {
                        "id": 4,
                        "type": "market",
                        "default": false,
                        "name": "陳小美",
                        "phone": "0911123456",
                        "address": "(019000)全家台中萬寶店"
                    },
                    {
                        "id": 5,
                        "type": "market",
                        "default": false,
                        "name": "陳小美",
                        "phone": "0911123456",
                        "address": "(087778)全家台中通通店"
                    }
                ]
            },
            "invoice": {
                "type": "member",
                "ext": {
                    "code": "0988999999"
                }
            }
        }';
    }
}
