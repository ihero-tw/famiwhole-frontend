<?php

namespace App\Http\Controllers\Auth;

use App\Http\Resources\MarketResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\RuntimeException;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class RegisterController extends Controller
{
  public function __construct() {
      //
  }

  public function create(Request $request, $shop)
  {
    return Inertia::render('Auth/Register', [
      'market' => new MarketResource($shop->markets->first()),
    ]);
  }

  public function store(Request $request)
  {
    try {
      //
      return Redirect::back()->with('success', '商品已加入購物車');
    } catch (RuntimeException $e) {
        return Redirect::back()->with('error', $e->getMessage());
    }
  }
}
