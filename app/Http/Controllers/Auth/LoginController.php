<?php

namespace App\Http\Controllers\Auth;

use App\Http\Resources\MarketResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\RuntimeException;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class LoginController extends Controller
{
  public function __construct() {
      //
  }

  public function showLoginForm(Request $request, $shop)
  {
    return Inertia::render('Auth/Login', [
      'market' => new MarketResource($shop->markets->first()),
    ]);
  }

  public function logout(Request $request, $shop)
  {
    return Inertia::render('Auth/Login', [
      'market' => new MarketResource($shop->markets->first()),
    ]);
  }

  public function forgotPassword(Request $request, $shop)
  {
    return Inertia::render('Auth/ForgotPassword', [
      'market' => new MarketResource($shop->markets->first()),
    ]);
  }

  public function resetPassword(Request $request, $shop)
  {
    return Inertia::render('Auth/ResetPassword', [
      'market' => new MarketResource($shop->markets->first()),
    ]);
  }

  public function login(Request $request)
  {
    try {
      //
      return Redirect::back()->with('success', '商品已加入購物車');
    } catch (RuntimeException $e) {
        return Redirect::back()->with('error', $e->getMessage());
    }
  }
}
