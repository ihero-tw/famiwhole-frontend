<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return  [
            'no' => $this->no,
            'name' => $this->name,
            'icon' => $this->icon,
            'logo' => $this->logo,
            'uri' => webPath('/'),
            'intro' => $this->intro,
            'address' => $this->contact->address,
            'phone' => $this->contact->phone,
            'email' => $this->contact->email,
            'socialMedias' => [
                'links' => $this->getSocialMedias(),
                'plugin' => $this->social_medias->plugin
            ]
        ];
    }

    private function getSocialMedias()
    {
        return collect($this->social_medias->links)->where('action', 1)->map(function ($item) {
            return [
                "type" => $item->type,
                "uri" => $item->uri
            ];
        })->all();
    }
}
