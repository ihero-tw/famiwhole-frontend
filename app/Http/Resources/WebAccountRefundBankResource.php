<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WebAccountRefundBankResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'bank' => [
                'code' => '115',
                'name' => '基隆市第二信用合作社',
                'branch' => [
                    'code' => '0096',
                    'name' => '暖暖分社',
                ],
            ],
            'account' => [
                'number' => '01641205207812',
                'name' => '王小雪'
            ],
            'identity' => [
                'type' => 'personal',
                'ext' => '83462647',
            ]
        ];
    }
}
