<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShoppingCartPreviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'identity' => $this->identity,
            'name' => $this->product->name,
            'photo' => $this->product->medias,
            'qty' => $this->qty,
            'model' => [
                'name' => $this->name,
                'price' => $this->price_special
            ]
        ];
    }
}
