<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MarketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'no' => $this->no,
            'name' => $this->name,
            'url' => sprintf("%s/market/%s/products", webPath(), $this->no),
            'intro' => $this->intro
        ];
    }
}
