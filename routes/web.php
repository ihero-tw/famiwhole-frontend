<?php

use App\Http\Controllers\Web\HomeController;
use App\Http\Controllers\Web\ProductController;
use App\Http\Controllers\Web\CartItemController;
use App\Http\Controllers\Web\ShoppingController;
use App\Http\Controllers\Web\MarketsController;
use App\Http\Controllers\Web\ContactController;
use App\Http\Controllers\Web\AccountController;
use App\Http\Controllers\Web\AccountAddresseeController;
use App\Http\Controllers\Web\AccountRefundBankController;
use App\Http\Controllers\Web\OrdersController;
use App\Http\Controllers\Web\RefundsController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;


use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('{shop}')->group(function () {
    Route::get('/', function () {
        return redirect(webPath(sprintf('%s/home', request()->shop->no)));
    });
    Route::prefix('home')->group(function () {
        Route::get('/', [HomeController::class, 'index'])->name('home');
    });
    Route::prefix('product')->group(function () {
        Route::get('{id}', [ProductController::class, 'index'])->name('product');
    });

    Route::prefix('market')->group(function () {
        Route::prefix('{market}')->group(function () {
            Route::get('/', function () {
                return redirect(webPath(sprintf('%s/market/%s/products', request()->shop->no, request()->market->no)));
            });
            Route::prefix('products')->group(function () {
                Route::get('/', [MarketsController::class, 'index'])->name('market.products');
                Route::get('{id}', [ProductController::class, 'index'])->name('market.products.show');
            });
        });
    });

    Route::get('search/{text}', [ProductController::class, 'search'])->name('search.product');

    Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');

    Route::prefix('forgot-password')->group(function () {
        Route::get('/', [LoginController::class, 'forgotPassword'])->name('web.login.password.request');
        Route::get('/email', [LoginController::class, 'forgotPassword'])->name('web.login.password.finish');
        Route::get('/reset', [LoginController::class, 'resetPassword'])->name('web.login.password.reset');
    });

    Route::get('register', [RegisterController::class, 'create'])->name('member.register');
    // Route::get('forget', [memberForgetController::class, 'index'])->name('member.forget');


    Route::prefix('shopping')->name('shopping.')->group(function () {
        Route::get('/checkout', [ShoppingController::class, 'showCheckout'])->name('checkout');
        Route::get('/done', [ShoppingController::class, 'done'])->name('done');

        Route::prefix('cart')->group(function () {
            Route::get('/', [ShoppingController::class, 'cart'])->name('cart');

            Route::prefix('items')->group(function () {
                Route::post('/', [CartItemController::class, 'store'])->name('cart.items.store');

                Route::prefix('{identity}')->group(function () {
                    Route::put('/', [CartItemController::class, 'update'])->name('cart.items.update');
                    Route::delete('/', [CartItemController::class, 'destroy'])->name('cart.items.destroy');
                });
            });
        });

        Route::prefix('checkout')->group(function () {
            Route::post('/', [ShoppingController::class, 'generateCheckout'])->name('checkout');

            Route::prefix('{uuid}')->group(function () {
                Route::get('/', [ShoppingController::class, 'showCheckout'])->name('checkout.show');
            });
        });
    });

    Route::prefix('account')->name('account.')->group(function () {
        Route::get('/', [AccountController::class, 'index'])->name('index');
        Route::put('/', [AccountController::class, 'update'])->name('update');

        Route::prefix('addressee')->name('addressee.')->group(function () {
            Route::get('/', [AccountAddresseeController::class, 'index'])->name('index');
            Route::post('/', [AccountAddresseeController::class, 'store'])->name('store');

            Route::prefix('{id}')->group(function () {
                Route::put('/', [AccountAddresseeController::class, 'update'])->name('update');
                Route::delete('/', [AccountAddresseeController::class, 'destroy'])->name('destroy');
            });
        });

        Route::prefix('refund')->name('refund.')->group(function () {
            Route::get('/', [AccountRefundBankController::class, 'index'])->name('index');
            Route::get('/edit', [AccountRefundBankController::class, 'edit'])->name('edit');
            Route::put('/', [AccountRefundBankController::class, 'update'])->name('update');
        });
    });

    Route::prefix('orders')->name('orders.')->group(function () {
        Route::get('/', [OrdersController::class, 'index'])->name('index');
        Route::get('{id}/edit', [OrdersController::class, 'edit'])->name('edit');
        Route::put('{id}/', [OrdersController::class, 'cancel'])->name('cancel');
    });

    Route::prefix('refunds')->name('refunds.')->group(function () {
        Route::get('/', [RefundsController::class, 'index'])->name('index');
        Route::post('/', [RefundsController::class, 'store'])->name('store');

        Route::prefix('{id}')->group(function () {
            Route::get('/', [RefundsController::class, 'edit'])->name('edit');
            Route::put('/', [RefundsController::class, 'update'])->name('update');
        });
    });

    Route::get('contact', [ContactController::class, 'index'])->name('index');
});
