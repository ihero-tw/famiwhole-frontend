工作列表
https://docs.google.com/spreadsheets/d/1U5wS2SC4eyM3OlHZzY1AoyFtBWM1wu1JmzEmoT21UjE/edit?usp=sharing

網站路徑
https://{Your Domain}/goodsupply/999999/home
http://plus.midmarket.com.tw/FamiWholesale/FamiWholesale_frontend/style2/

git
https://bitbucket.org/ihero-tw/famiwhole-frontend/src
location:9100
Vue 目錄路徑及說明

```
resources/js
    - Admin                     : 管理平台目錄
        - Layouts               : 平台的 Layout
        - Pages                 : 頁面目錄
            - Accounts          : 帳號管理頁
                - Components    : 頁面內的通用元件目錄
        - Shared                : 平台內的通用元件目錄
    - Supplier                  : 供應商平台目錄
    - Shop                      : 店家平台目錄
    - Web                       : 賣場平台目錄
    - Utils                     : 應用程式通用元件、程式目錄
```

### 目前已知問題及處理方式

1. 廠商樣版是用 jquery + bootstrap 但會造成 vue 無法處理 jquery 事件。因此我們另外安裝 bootstrap 5 用於處理 bootstrap 所提供的元件及特效。所以有相關應用時，需參考 bootstrap 5 官方文件。程式碼可以參考一下 `resources/js/Utils/Components/Modal`

2. 一些原樣版用放置 Public 的靜態檔案 如 js 及 css，放置於 `public/frontend` 之下

``` js
// sample
this.$swal('Hello Vue world!!!');

// 取得 Controller 的 data 值 ! mounted !
this.$page.props.products
route('web.product', [this.$page.props.shop.no, this.id])

// setup 使用 響應式
// watch 監聽一個 watchEffect 監聽全部
import { reactive, ref, toRefs, getCurrentInstance, computed, onMounted, watch, watchEffect } from 'vue'
setup(props, context) {
    const app = getCurrentInstance()
    const $this = app.appContext.config.globalProperties
    const mixin = app.proxy.baseUrl
    let test = computed(() => 123)

    const search2 = ref('');
    watch(search2, () => {
        console.log('watch search2', search2.value)
        context.emit('value', search2.value)
    })
    watchEffect(() => {
        console.log('watchEffect', test, search2.value)
    })
    onMounted(() => {
        console.log(app)
    })
    return {test}
}

// fetch http request
/** 加入購物車 or 立即購買 **/
this.$inertia.form({
    item: {
        id: '5157424d597847674c64-43505359565a654a6758', // Model uuid
        qty: 1 // 數量
    },
    }).post(this.route('web.shopping.cart.items.store', this.$page.props.shop.data.no), {
    preserveScroll: true,
})
/** 刪除購物車商品 **/
// /goodsupply/999999/shopping/cart/items/destroy
this.$inertia.delete(this.route('web.shopping.cart.items.destroy', [
    this.$page.props.shop.data.no, cart.item.identity
]), {
    preserveScroll: true
})

/** 會員資料變數位置 **/
````
    props.account
````

/** 會員資料儲存 **/
this.$inertia.form({
        name: 'aj',
        email: 'aj@gmail.com',
        password: '1234',
        password_confirm: '1234',
        password_change: true
    }).put('/goodsupply/999999/account', {
    preserveScroll: true,
})

/** 收件資訊變數位置 **/
````
    props.addressees

    資料內的 type 變數：
    home 表示放 "地址" 分頁
    market 表示放 "店鋪" 分頁
````

/** 收件資訊 - 新增 **/
this.$inertia.form({
        name: 'aj',
        phone: '1234',
        default: true,
        address: '(018015) 全家台中和順店'
    }).post('/goodsupply/999999/account/addressee', {
    preserveScroll: true,
})
/** 收件資訊 - 更新 **/
this.$inertia.form({
        name: 'aj',
        phone: '1234',
        default: true,
        address: '(018015) 全家台中和順店'
    }).put('/goodsupply/999999/account/addressee/1'), {
    preserveScroll: true,
})
/** 收件資訊 - 刪除 **/
this.$inertia.delete('/goodsupply/999999/account/addressee/1'), {
    preserveScroll: true
})

/** 退款帳戶變數位置 **/
````
    props.refund

    identity 資料內的 type 變數：
    personal 表示 "個人戶"
    business 表示 "商業戶"
    foreign 表示 "外籍人士"
````

/** 退款帳戶 - 更新 **/
this.$inertia.form({
        identity_type: 'personal', // 身分別，型態有 personal(個人戶), business(商業戶), foreign(外籍人士)
        identity_ext: '1234',   // 在介面上的這些欄位：身份證號, 統一編號, 居留證號碼 輸入的值都放這裡
        bank_code: '115',
        bank_name: '基隆市第二信用合作社',
        bank_branch_code: '0096',
        bank_branch_name: '暖暖分社',
        bank_account: '01641205207812',
        bank_account_name: '王小雪'
    }).put('/goodsupply/999999/account/refund'), {
    preserveScroll: true,
})

/** 訂單列表頁 - 變數位置 **/
````
    props.orders
````

/** 訂單內頁 - 變數位置 **/
````
    props.order

    props.order.info 訂單資訊
    props.order.items 訂單內的項目，依照供應商進行分群
    props.order.items[1].info 出貨資訊
    props.order.items[1].unshipped 還沒出貨的商品列表
    props.order.items[1].shipments 已出貨的出貨單列表
    props.order.items[1].shipments[1].number 包裹查詢編號
    props.order.items[1].shipments[1].stage 運送階段，用來顯示進度條的

    props.order.items[1].shipments[1].canReturn
    消費者已取貨後此狀態會變成 true,
    介面要顯示 "退貨說明" 及 "退貨申請" 二個按鈕

    props.order.items[1].shipments[1].appliedReturn
    消費者進行退貨申請此狀態會變成 true,
    介面上的 "退貨申請" 按鈕要隱藏起來
````

/** 訂單內頁 - 訂單取消 **/
this.$inertia.form({
        reason: '想要或已購買類似商品', // 取消原因
        note: '想換別的可以嗎？' // 備註
    }).put('/goodsupply/999999/orders/6000220339'), {
    preserveScroll: true,
})

/** 訂單內頁 - 退貨申請 **/
this.$inertia.form({
        shipmentNo: 'S20987654321',
        items: [
            {
                id: 1, // 項目 id
                reason: '尺寸版型不合/顏色不喜歡', // 退貨原因
                note: '寄錯了', // 備註
                amount: 1, // 退貨數量
                files: [] // 上傳的檔案
            }
        ]
    }).post('/goodsupply/999999/refunds'), {
    preserveScroll: true,
})

/** 退貨單列表頁 - 變數位置 **/
````
    props.refunds
````

/** 退貨單內頁 - 變數位置 **/
````
    props.refund

    props.refund.refund 退款資料，銀行、分行、帳戶名稱... 等資料
    props.refund.stage 退貨進度條
    props.refund.stageExamine 商品驗退階段，內容有：pending (未驗收), examined (平台驗收), agreed (買家同意驗收結果), adjudication (平台裁決), completed (已完成)，當遇到 examined 狀態時顯示 "回覆退貨駁回" 按鈕

    // 顯示在 "買家 申請退貨/退款原因"
    props.refund.items[1].returned.causer.reason 退貨原因
    props.refund.items[1].returned.causer.note 退貨備註/說明
    props.refund.items[1].returned.photos 退貨時上傳的圖片

    // 顯示在 "平台 退貨/退款處理"
    props.refund.items[1].examined.amount.successful 平台驗收成功的數量
    props.refund.items[1].examined.amount.failure 平台驗收失敗的數量
    props.refund.items[1].examined.explain 平台驗收失敗的說明
    props.refund.items[1].examined.photos 平台驗收失敗上傳的圖片

    // 顯示在 "買家 回覆"
    props.refund.items[1].agreed.amount.successful 買家同意驗收結果的數量
    props.refund.items[1].agreed.amount.failure 買家不同意驗收結果的數量
    props.refund.items[1].agreed.explain 買家不同意驗收結果的說明

    // 顯示在 "平台 裁決"
    props.refund.items[1].adjudication.amount.successful 平台裁決結果的成功數量
    props.refund.items[1].adjudication.amount.failure 平台裁決結果的失敗數量
    props.refund.items[1].adjudication.explain 平台裁決結果說明
````


/** 退貨單內頁 - 買家回覆驗收結果**/
this.$inertia.form([
    {
        id: 1, // 項目 id
        amount: 1, // 同意退貨成功數量
        explain: '我有寄回去哦', // 說明
    }, {
        id: 2, // 項目 id
        amount: 3, // 同意退貨成功數量
        explain: '我忘了寄回去了', // 說明
    }
]).put('/goodsupply/999999/refunds/S20987654324'), {
    preserveScroll: true,
})


/** 購物車頁 - 變數位置 **/
````
    props.shoppingListing.data 陣列資料，以供應商做為群組分類
    props.shoppingListing.data[1].supplier 供應商名稱
    props.shoppingListing.data[1].count 群組內的商品數量
    props.shoppingListing.data[1].items 群組內的商品列表
    props.shoppingListing.data[1].items[1].identity 購物車商品編號
    props.shoppingListing.data[1].items[1].model 商品規格、庫存、價格
    props.shoppingListing.data[1].items[1].product 商品基本資訊
    props.shoppingListing.data[1].items[1].qty 購買數量

    說明：
    1. 購物車數量異動時都要回傳後端更新購物內資料
    2. 購物車數量不能點擊超過庫存
    3. 購物車數量點擊小於 1 時要跳出是否從購物刪除的提示視窗, 數量不能為 0
    4. 購物車數量異動時，結帳總金額要從新計算
    5. `前往結帳` 按鈕要判斷是否登入中去切換功能，未登入要跳出登入 `Modal` 並由使用者選擇要登入或首次購物，如點首次購物就執行 form post。如一開始就是登入就直接執行 form post。判斷方式利用 `props.user` 變數判斷
````
/** 購物車頁 - 送出結帳商品 **/
this.$inertia.form({
    identities: [
        "73686f7070696e672d3136",
        "83686f8070699e172d32",
    ],
}).post('/goodsupply/999999/shopping/checkout/', {
    preserveScroll: true,
})
/** 購物車頁 - 購物車數量更新 **/
this.$inertia.form({
    qty: 1
}).put('/goodsupply/999999/shopping/cart/items/{items[1].identity}', {
    preserveScroll: true,
})
/** 購物車頁 - 刪除購物車商品 **/
this.$inertia.delete('/goodsupply/999999/shopping/cart/items/{items[1].identity}', {
    preserveScroll: true
})


/** 購物填寫資料頁 - 變數位置 **/
````
    props.checkout 帳單內容
    props.checkout.items.data 物品資訊，以供應商做為群組分類
    props.checkout.items.data[1].identity 群組編號
    props.checkout.items.data[1].supplier 供應商名稱
    props.checkout.items.data[1].count 群組內的商品數量
    props.checkout.items.data[1].items 群組內的商品列表
    props.checkout.items.data[1].payable 應付費用
    props.checkout.items.data[1].payable.fee 運費
    props.checkout.items.data[1].payable.subtotal 商品小計
    props.checkout.items.data[1].payable.total 金額總計
    props.checkout.items.data[1].shippingMethods 配送方式
    props.checkout.items.data[1].shippingMethods[1].id 配送方式編號
    props.checkout.items.data[1].shippingMethods[1].title 標題
    props.checkout.items.data[1].shippingMethods[1].type 類型 (對應 `props.user.addressees.data[1].type`)
    props.checkout.items.data[1].shippingMethods[1].notice 注意事項
    props.checkout.paymentMethods.data 付款方式
    props.checkout.paymentMethods.data[1].id 付款方式編號
    props.checkout.paymentMethods.data[1].title 標題
    props.checkout.paymentMethods.data[1].notice 注意事項
    props.checkout.total 應付總金額

    props.user.addressees.data 地址簿
    props.user.addressees.data[1].id 編號
    props.user.addressees.data[1].type 類型 (`home`、`market`)
    props.user.addressees.data[1].default 是否預設地址
    props.user.addressees.data[1].name 收件人姓名
    props.user.addressees.data[1].phone 電話
    props.user.addressees.data[1].address 地址

    props.user.invoice 發票資訊
    props.user.invoice.type 發票類型 (會員載具: member, 公司戶電子發票: company, 捐贈: donation, 手機載具條碼: barcode, 自然人憑證載具: citizen)
    props.user.invoice.ext 發票內容

    說明：
    一、介面有分成 `首次購物` 和 `登入購物` 二個介面，不同的介面 UI 動作會不一樣
        首次購物：
            1. 當 `props.user` 是 null 時為首次購物
            2. 顯示訂購人資訊
            3. 新增收件資訊的介面流程是 `點擊新增店鋪` -> `跳出新增店舖 Modal (預設取貨店舖 Checkbox 不顯示)` -> `點擊確認新增` -> `資料帶入 (新增店鋪按鈕消失、變更按鈕出現)`
            4. 修改收件資訊的介面流程是 `點擊變更按鈕` -> `跳出新增店舖 Modal (預設取貨店舖 Checkbox 不顯示)` -> `點擊確認新增` -> `資料帶入`
            5. 首次購物在送出訂單資訊時必需帶入訂購人資訊 `consumer` 變數

        登入購物：
            1. 當 `props.user` 不是 null 時為登入購物
            2. 不顯示訂購人資訊
            3. 自動帶入預設收件人，如果 `props.user.addressees` 沒有對應的收件資訊時顯示 `新增店鋪` 按鈕
            4. 新增收件資訊的介面流程是 `點擊新增店鋪` -> `跳出新增店舖 Modal` -> `點擊確認新增` -> `資料帶入 (新增店鋪按鈕消失、變更按鈕出現)`
            5. 修改收件資訊的介面流程是 `點擊變更按鈕` -> `跳出選取收件店舖選單 Modal` -> `點擊新增或編輯` -> `跳出新增或編輯店舖 Modal` -> `點擊儲存或確認新增` -> `跳出新增或編輯店舖 Modal` -> `選擇後點擊選擇按鈕` -> `資料帶入`

    二、收件資訊
        1. 收件資訊會依照選擇的 `配送方式` 載入不同的資訊，判斷方式：利用 `props.checkout.items.data[1].shippingMethods[1].type` 看是 `home` 還是 `market`，再去地址簿載入相對應的資料。
        2. `配送方式` type如果是 `home` 按鈕顯示 `新增地址`，反之 `配送方式` type如果是 `market` 按鈕顯示 `新增店鋪`
        3. `配送方式` type如果是 `home` 跳出來的 `新增地址 Modal`，反之 `配送方式` type如果是 `market` 跳出來的 `新增店鋪 Modal`
        4. 收件資訊選擇後最終會跟著 form 表單送出，送出的資料格式放在請放 `recipient` 變數，資料並分別放置 `name`、`phone`、`address`。最終資料組成如下：
            recipient: {
                name: '',
                phone: '',
                address: '',
            }

    三、發票
        1.  選項 會員載具 顯示 會員手機載具 欄位
            選項 公司戶電子發票 顯示 抬頭、統編 欄位
            選項 捐贈 顯示 愛心碼及代碼查詢 欄位
            選項 手機載具條碼 顯示 手機載具條碼 欄位
            選項 自然人憑證載具 顯示 16碼大寫英數字 欄位

        2.  `props.user.invoice.ext` JSON 格式：
            公司戶電子發票:
            {
                title:
                tex_number:
            }
            會員載具、捐贈...等：
            {
                code:
            }
        3. `props.user.invoice.type` 如果等於 `null` 表示消費者未填寫，介面選單請自動帶入第一個選項。發票類型有 (會員載具: member, 公司戶電子發票: company, 捐贈: donation, 手機載具條碼: barcode, 自然人憑證載具: citizen)

    四、localstorage：
        1. 送出後發生錯誤後要返回購物車頁，所以必須保留當時的訂購資訊並再回到 `購物填寫資料頁` 將資料帶回去
        2. 需寫入 localstorage 的欄位有：
            - 首次購物的訂購人資訊
            - 發票資料
            - 配送方式及收件資訊、備註
            - 付款方式
````
/** 購物填寫資料頁 - 送出資料 **/
this.$inertia.form({
    uuid: this.$page.props.checkout.uuid,
    consumer: { // 首頁購物的會員才需要傳
        name: '',
        phone: '',
        email: '',
        password: '',
        password_confirmation: '',
    },
    paymentMethod: 1,
    invoice: {
        type: 'member',
        ext: {
            code: '0988999999'
        }
    },
    shipments: [ // 每一個商品群組就是一個 shipment、分別都需要 identity、運送方法、收件人、備註 變數
        {
            identity: "737570706c6965722d31",
            shippingMethods: 1,
            recipient: {
                name: '',
                phone: '',
                address: '',
            },
            notes: "早點出貨"
        },
        {
            identity: "737570706c6965722d3139",
            shippingMethods: 1,
            recipient: {
                name: '',
                phone: '',
                address: '',
            },
            notes: ''
        },
    ]
}).post('/goodsupply/999999/orders'), {
    only: ['checkout'],
    preserveScroll: true,
})

/** 購物填寫資料頁 - 收件資訊 - 新增 **/
this.$inertia.form({
    name: 'aj',
    phone: '1234',
    default: true,
    type: 'market'
    address: '(018015) 全家台中和順店'
}).post('/goodsupply/999999/account/addressee', {
    only: ['user'],
    preserveScroll: true,
})

/** 購物填寫資料頁 - 收件資訊 - 更新 **/
this.$inertia.form({
    name: 'aj',
    phone: '1234',
    default: true,
    address: '(018015) 全家台中和順店'
}).put('/goodsupply/999999/account/addressee/{props.user.addressees.data[1].id}'), {
    only: ['user'],
    preserveScroll: true,
})



this.form.post('/user/profile-information', {
    preserveScroll: true
})

if (this.$page.props.flash.notify.success) {}
```

```regex
<img.*?src="images/(.*?)".*?> => <Img src="/frontend/images/style3/$1" />
```

```php
Route::get('/member/{member}/show', [MemberController::class, 'show'])->name('member.show');
// html
// <InertiaLink :href="route('member.show', { id: member.id })">
//  show
// </InertiaLink>

// this.$inertia.visit(this.baseUrl + 'search', { preserveScroll: true })
```

(Vue3重構購物車)[https://www.gushiciku.cn/pl/p3tg/zh-tw]

### 對照
member_my-account.html -> account
member_my-addresse.html -> account/addressee
member_my-orders.html -> orders
member_my-order-detail.html -> orders/{no}/edit
member_my-return.html -> refunds
member_my-return-detail.html -> refunds/{no}/edit
member_my-refund.html -> account/bank
member_my-refund-edit.html -> account/bank/edit

### Route listing
```
+--------+---------------+---------------------------------------------+-----------------------------------+--------------------------------------------------------------------------------+------------------------------------------------------+
| Domain | Method        | URI                                         | Name                              | Action                                                                         | Middleware                                           |
+--------+---------------+---------------------------------------------+-----------------------------------+--------------------------------------------------------------------------------+------------------------------------------------------+
|        | GET|HEAD      | home                                        | web.home                          | App\Http\Controllers\HomeController@index                                      | web                                                  |
|        | GET|HEAD      | market/{id}/products                        | web.market                        | App\Http\Controllers\MarketsController@index                                   | web                                                  |
|        | GET|HEAD      | market/{id}/products/{id}                   | web.market.product                | App\Http\Controllers\ProductsController@show                                   | web                                                  |
|        | GET|HEAD      | products/search                             | web.product.search                | App\Http\Controllers\ProductsController@search                                 | web                                                  |

|        | GET|HEAD      | login                                       | web.login                         | App\Http\Controllers\Auth\LoginController@showLoginForm                        | web,guest                                            |
|        | POST          | login                                       | web.login                         | App\Http\Controllers\Auth\LoginController@login                                | web,guest                                            |
|        | GET|HEAD      | logout                                      | web.logout                        | App\Http\Controllers\Auth\LoginController@logout                               | web,auth                                             |
|        | GET|HEAD      | forgot-password                             | web.login.password.request        | App\Http\Controllers\Auth\LoginController@forgotPassword                       | web,guest                                            |
|        | POST          | forgot-password                             | web.login.password.email          | App\Http\Controllers\Auth\LoginController@resetLink                            | web,guest                                            |
|        | GET|HEAD      | register                                    | web.register                      | App\Http\Controllers\Auth\RegisteredUserController@create                      | web,guest                                            |
|        | POST          | register                                    | web.register                      | App\Http\Controllers\Auth\RegisteredUserController@store                       | web,guest                                            |

|        | GET|HEAD      | account                                     | web.account                       | App\Http\Controllers\AccountController@index                                   | web,auth                                             |
|        | PUT|PATCH     | account                                     | web.account.update                | App\Http\Controllers\AccountController@update                                  | web,auth                                             |
|        | GET|HEAD      | account/addressee                           | web.account.addressee             | App\Http\Controllers\AccountAddresseeController@index                          | web,auth                                             |
|        | POST          | account/addressee                           | web.account.addressee             | App\Http\Controllers\AccountAddresseeController@store                          | web,auth                                             |
|        | GET|HEAD      | account/addressee/{no}                      | web.account.addressee.edit        | App\Http\Controllers\AccountAddresseeController@edit                           | web,auth                                             |
|        | PUT|PATCH     | account/addressee/{no}                      | web.account.addressee.update      | App\Http\Controllers\AccountAddresseeController@update                         | web,auth                                             |
|        | DELETE        | account/addressee/{no}                      | web.account.addressee.destroy     | App\Http\Controllers\AccountAddresseeController@destroy                        | web,auth                                             |
|        | GET|HEAD      | account/bank                                | web.account.bank                  | App\Http\Controllers\AccountBankController@index                               | web,auth                                             |
|        | PUT|PATCH     | account/bank/edit                           | web.account.bank.edit             | App\Http\Controllers\AccountBankController@edit                                | web,auth                                             |
|        | PUT|PATCH     | account/bank                                | web.account.bank.update           | App\Http\Controllers\AccountBankController@update                              | web,auth                                             |

|        | GET|HEAD      | orders                                      | web.orders                        | App\Http\Controllers\OrdersController@index                                    | web,auth                                             |
|        | PUT           | orders/{no}                                 | web.orders                        | App\Http\Controllers\OrdersController@update                                   | web,auth                                             |
|        | GET|HEAD      | orders/{no}/edit                            | web.orders.edit                   | App\Http\Controllers\OrdersController@edit                                     | web,auth                                             |
|        | POST          | orders/{no}/cancel                          | web.orders.cancel                 | App\Http\Controllers\OrdersController@cancel                                   | web,auth                                             |
|        | GET|HEAD      | orders/{no}/refunds                         | web.orders.refunds                | App\Http\Controllers\OrdersController@refunds                                  | web,auth                                             |
|        | POST          | orders/{no}/refunds                         | web.orders.refunds                | App\Http\Controllers\OrdersController@acceptRefunds                            | web,auth                                             |

|        | GET|HEAD      | refunds                                     | web.refunds                       | App\Http\Controllers\RefundsController@index                                   | web,auth                                             |
|        | GET|HEAD      | refunds/{no}/edit                           | web.refunds.edit                  | App\Http\Controllers\RefundsController@update                                  | web,auth                                             |

|        | GET|HEAD      | shopping/cart                               | web.cart                          | App\Http\Controllers\ShoppingController@cart                                   | web,auth                                             |
|        | GET|HEAD      | shopping/cart/items                         | web.cart.items                    | App\Http\Controllers\CartItemController@index                                  | web,auth                                             |
|        | POST          | shopping/cart/items                         | web.cart.items.store              | App\Http\Controllers\CartItemController@store                                  | web,auth                                             |
|        | PUT           | shopping/cart/items/{id}                    | web.cart.items.update             | App\Http\Controllers\CartItemController@update                                 | web,auth                                             |
|        | DELETE        | shopping/cart/items/{id}                    | web.cart.items.destroy            | App\Http\Controllers\CartItemController@destroy                                | web,auth                                             |
|        | GET|HEAD      | shopping/checkout                           | web.cart                          | App\Http\Controllers\ShoppingController@checkout                               | web,auth                                             |
|        | GET|HEAD      | shopping/done                               | web.cart                          | App\Http\Controllers\ShoppingController@done                                   | web,auth                                             |
+--------+---------------+---------------------------------------------+-----------------------------------+--------------------------------------------------------------------------------+------------------------------------------------------+
```
```js
methods: {
    testPostToCheckout () {
        this.$inertia.form({
        identities: [
            "73686f7070696e672d3136",
        ],
        }).post('/goodsupply/999999/shopping/checkout/', {
        preserveScroll: true
        })
    },
    testPutQty () {
        this.$inertia.form({
        qty: 10,
        }).put('/goodsupply/999999/shopping/cart/items/312d31', {
        preserveScroll: true
        })
    }
}
```