const path = require('path');

const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');

module.exports = {
  plugins: [
    new CaseSensitivePathsPlugin()
  ],
  resolve: {
    alias: {
      '@': path.resolve('resources/js'),
    },
  },
};
