<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title inertia>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="/frontend/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/frontend/vendor/animate/animate.css">
    <!-- Fonts -->
    <link rel="stylesheet" href="/frontend/vendor/fonts/css/font-awesome.min.css">
    <!-- Customize -->
    <link rel="stylesheet" href="/frontend/css/theme.css?20210906">
    <link rel="stylesheet" href="/frontend/css/custom.css?20210906">
    <link rel="stylesheet" href="/frontend/css/memberarea.css?20210906">
    <link rel="stylesheet" href="/frontend/css/style02.css">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <!-- Scripts -->
    @routes
    <script src="/frontend/vendor/jquery/jquery.min.js"></script>
    <script src="/frontend/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="/frontend/vendor/easing/jquery.easing.min.js"></script>

    <!-- Swiper -->
    <link rel="stylesheet" href="/frontend/vendor/swiper-bundle/swiper-bundle.min.css">
    <script src="/frontend/vendor/swiper-bundle/swiper-bundle.min.js"></script>

    <!-- owl -->
    <link rel="stylesheet" href="/frontend/vendor/owlcarousel/dist/css/owl.carousel.min.css">
    <script src="/frontend/vendor/owlcarousel/dist/owl.carousel.min.js"></script>

    <!-- magnific -->
    <link rel="stylesheet" href="/frontend/vendor/magnific-popup/magnific-popup.css">
    <script src="/frontend/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Customize -->
    {{-- <script src="/frontend/js/theme.js"></script> --}}
    <script src="{{ mix('js/web/app.js') }}" defer></script>
  </head>
  <body class="font-sans antialiased" data-backUrl="/goodsupply/999999/" data-webpath="{{ subdirRoute('/', 'web') }}">
    @inertia
  </body>
</html>
