require('./bootstrap');

// Import modules...
import { createApp, h } from 'vue';
import { App as InertiaApp, plugin as InertiaPlugin } from '@inertiajs/inertia-vue3';
import { Head, Link } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
import VueFeather from 'vue-feather'
import store from './Store'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css'
import Vue3MobileDetection from "vue3-mobile-detection";

const el = document.getElementById('app');

createApp({
  render: () =>
    h(InertiaApp, {
      initialPage: JSON.parse(el.dataset.page),
      resolveComponent: (name) => require(`./Pages/${name}`).default,
    }),
})
  .mixin({ methods: { route } })
  .component(VueFeather.name, VueFeather)
  .component('InertiaHead', Head)
  .component('InertiaLink', Link)
  .use(store)
  .use(InertiaPlugin)
  .use(VueSweetalert2)
  .use(Vue3MobileDetection)
  .mount(el);

InertiaProgress.init({ color: '#4B5563' });
