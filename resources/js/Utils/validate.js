function invalid(input, type) {
  if (input.$errors[0]) {
    if (input.$errors[0].$validator == type) {
      return true
    }
  }
  return false
}

export { invalid };
