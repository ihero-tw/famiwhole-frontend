import Echo from 'laravel-echo'

const client = require('socket.io-client');

const echo = {
  listener (channel, token, type) {
    const host = process.env.MIX_ECHO_SERVER_HOST ? process.env.MIX_ECHO_SERVER_HOST: window.location.hostname
    const echo = new Echo({
      client: client,
      broadcaster: 'socket.io',
      host: host,
      path: process.env.MIX_ECHO_SERVER_PATH,
      auth: {
        headers: {
          Type: type,
          Authorization: 'Bearer ' + token
        }
      }
    })

    return echo.private(channel)
  }
}

export default echo