import _ from "lodash";

// initial state
const state = () => ({
  fields: [],
})

// getters
const getters = {
  exist: (state) => (fieldName) => state.fields.find(item => item.name === fieldName),
  getValue: (state) => (fieldName) => {
    const foundIndex = state.fields.findIndex(item => item.name === fieldName)
    if (foundIndex > -1) {
      return state.fields[foundIndex].value
    } else {
      return null
    }
  },
  getSearchString: (state) => {
    let fields = []
    let query = []

    state.fields.forEach(item => {
      if (item.name === 'ask') {
        fields.push(`${ item.name }=${ item.value }`)
      } else if (item.value) {
        if (item.name.indexOf(',') != -1) {
          let bucket = []
          const names = _.split(item.name, ',')
          names.forEach(name => {
            bucket.push(`${ name }:${ item.value }`)
          })
          query.push(bucket.join(','))
        } else {
          query.push(`${ item.name }:${ item.value }`)
        }
      }
    })

    return query.join(';') + (_.isEmpty(fields) ? '': '&' + fields.join('&'))
  }
}

// actions
const actions = {}

// mutations
const mutations = {
  parser (state, string) {
    const fields = _.compact(_.split(string, ';'))
    state.fields = fields.map(function(item) {
      if (item.indexOf(',') != -1) {
        let name = []
        let value = ''

        const items = _.split(item, ',')
        items.map(function(item) {
          let itemAry = _.split(item, ':')
          let pulled = _.pullAt(itemAry, 0)

          item = _.zipObject(['name', 'value'], [_.head(pulled), itemAry.join(':')])
          name.push(item.name)
          value = item.value
        })

        return {
          'name': name.join(','),
          'value': value
        }
      } else {
        let itemAry = _.split(item, ':')
        let pulled = _.pullAt(itemAry, 0)

        return _.zipObject(['name', 'value'], [_.head(pulled), itemAry.join(':')])
      }
    })
  },
  setValue (state, field) {
    const foundIndex = state.fields.findIndex(item => item.name === field.name)
    if (foundIndex > -1) {
      state.fields[foundIndex] = field
    } else {
      state.fields.push(field)
    }
  },
  destroy (state, field) {
    const result = state.fields.filter(function(item) {
      if (item.name != field.name){
        return true
      }
    })
    state.fields = result
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}