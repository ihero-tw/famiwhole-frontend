
export const mixinFilter = {
  props: {
    routeName: String,
  },

  data() {
    return {
      form: {
        search: '',
        listingLength: this.$page.props.filters.listingLength ?? 5,
        sorting: {
          type: this.$page.props.filters.sorting?.type ?? 'asc',
          column: this.$page.props.filters.sorting?.column ?? ''
        },
      },
      searchUrl: this.route(this.$page.props.routeName)
    }
  },

  methods: {
    columnSorting (column) {
      this.form.search = this.$store.getters["filters/getSearchString"]
      this.form.sorting.column = column
      this.form.sorting.type = this.form.sorting.type == 'asc'? 'desc': 'asc'

      let query = _.pickBy(this.form)
      this.$inertia.get(
        this.searchUrl,
        Object.keys(query).length ? query : { remember: "forget" }, {
          preserveScroll: true,
          preserveState: false
        }
      )
    },
    isSortBy (type, column) {
      if (this.form.sorting.column) {
        return this.form.sorting.type == type && this.form.sorting.column == column
      }
      return null
    },
    lengthChanged () {
      this.form.search = this.$store.getters["filters/getSearchString"]

      let query = _.pickBy(this.form)
      this.$inertia.get(
        this.searchUrl,
        Object.keys(query).length ? query : { remember: "forget" }, {
          preserveScroll: true,
          preserveState: false
        }
      )
    },
    reset () {
      this.$inertia.get(this.searchUrl)
    },
    submit () {
      this.form.search = this.$store.getters["filters/getSearchString"]

      let query = _.pickBy(this.form)
      this.$inertia.get(
        this.searchUrl,
        Object.keys(query).length ? query : { remember: "forget" }, {
          preserveScroll: true,
          preserveState: false
        }
      )
    },
  }
}

