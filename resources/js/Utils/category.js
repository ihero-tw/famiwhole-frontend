import _ from "lodash";

function findUpperCategoryNames(categories, parent_id) {
  let upperCategoryNames = []
  let upperCategory = _.find(categories, item => item.id == parent_id )

  if(upperCategory == undefined) return upperCategoryNames
  if (upperCategory.parent_id != 0 ) {
    upperCategoryNames = findUpperCategoryNames(categories, upperCategory.parent_id)
  }
  upperCategoryNames.push(upperCategory.name)
  return upperCategoryNames
}


export {findUpperCategoryNames};
