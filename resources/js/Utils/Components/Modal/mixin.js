import { Modal } from "bootstrap"

export const mixinRefModal = {
  methods: {
    toggleModal (modalName) {
      this.$refs[modalName].mixinModalToggle()
    },
    hideModal (modalName) {
      this.$refs[modalName].mixinModalHide()
    },
    showModal (modalName) {
      this.$refs[modalName].mixinModalShow()
    }
  }
}

export const mixinModal = {
  data () {
    return {
      modalName: nanoid(),
      mixinModal: '',
    }
  },

  mounted () {
    const model = document.getElementById(this.modalName)
    model.addEventListener('hidden.bs.modal',  (event) => {
      this.reset()
    })

    this.mixinModal = new Modal(model)
  },

  provide() {
    return {
      modalName: this.modalName
    }
  },

  methods: {
    mixinModalToggle () {
      this.mixinModal.toggle()
    },
    mixinModalHide () {
      this.mixinModal.hide()
    },
    mixinModalShow () {
      this.mixinModal.show()
    },
    hideModal () {
      this.mixinModal.hide()
    },
    showModal () {
      this.mixinModal.show()
    },
    reset () {
      //
    }
  }
}
