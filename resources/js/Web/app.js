require('./bootstrap');

// Import modules...
import { createApp, h } from 'vue';
import { App as InertiaApp, plugin as InertiaPlugin } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
import 'sweetalert2/dist/sweetalert2.min.css'
import masterLayout from './Layouts/AppLayout';
import registerComponents from './Config/index.js';

const el = document.getElementById('app');
const app = createApp({
  render: () =>
    h(InertiaApp, {
      initialPage: JSON.parse(el.dataset.page),
      resolveComponent: (name) => require(`./Pages/${name}`).default,
    }),
});


app.mixin({methods: {route}})
app.component('masterLayout', masterLayout)
app.use(registerComponents)
app.mount(el)

InertiaProgress.init({ color: '#4B5563' });