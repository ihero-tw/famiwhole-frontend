import {props, fetchData} from '@/Web/Config/api/request'

// 取得會員帳號資料
export function getAccount() {
  let data = props() == null ? false : props()
  data = typeof data.account == 'undefined' ? {} : data.account
  data = typeof data.data == 'undefined' ? {email: '', name: '', phone: ''} : data.data
  console.log({account: data})
  return data
}

// 儲存會員帳號資料
export function saveAccount(form) {
  fetchData('put', 'account', form)
}

// 取得會員收件地址資料
export function getAddress() {
  let data = props() == null ? false : props()
  data = typeof data.addressees == 'undefined' ? {} : data.addressees
  data = typeof data.data == 'undefined' ? [] : data.data
  if (data.length == 0) return {home: [], market: []}
  let newData = {home: [], market: []}
  data.forEach(item => {
    if (item.type == 'home') newData.home.push(item)
    if (item.type == 'market') newData.market.push(item)
  })
  return newData
}

// 會員收件地址 新增 | 更新 | 刪除
export function changeAddress($type = 'update', data = null) {
  const id = typeof data.id !== 'undefined' ? data.id : ''
  if($type == 'update') {
    if (id == '') return console.error('id is empty')
    fetchData('put', 'account/addressee/' + id, data, function() {
      $(document).find('.modal').modal('toggle')
    })
  } else if ($type == 'insert') {
    fetchData('post', 'account/addressee/', data, function() {
      $(document).find('.modal').modal('toggle')
    })
  } else if ($type == 'del') {
    if (id == '') return console.error('id is empty')
    fetchData('delete', 'account/addressee/' + id + '/destroy', data)
  }
}

// 取得會員退款帳戶資料
export function getRefund() {
  let data = props() == null ? false : props()
  data = typeof data.refund == 'undefined' ? {} : data.refund
  data = typeof data.data == 'undefined' ? {account: {}, bank: {}, identity: {}} : data.data
  if (data.identity !== null) {
    if(typeof data.identity.type !== 'undefined') {
      const idType = data.identity.type
      if (idType == 'personal') data.identity.typeName = '個人戶'
      else if (idType == 'business') data.identity.typeName = '商業戶'
      else if (idType == 'foreign') data.identity.typeName = '外籍人士'
      else data.identity.typeName = ''
    }
  }
  if (data.bank !== null) {
    if(typeof data.bank.branch !== 'undefined') {
      const branch = data.bank.branch
      data.bank.bankName = branch.code + ' ' + branch.name
    }
  }
  return data
}

// 儲存會員退款帳戶資料
export function saveRefund(data) {
  console.log({saveRefund: data})
  fetchData('put', 'account/refund', data)
}

// 取得訂單資料列表
export function getOrder() {
  let data = props() == null ? false : props()
  data = typeof data.orders == 'undefined' ? {} : data.orders
  console.log({order: data})
  return data
}

// 取得訂單資料內容
export function getOrderDetail() {
  let data = props() == null ? false : props()
  data = typeof data.order == 'undefined' ? {} : data.order
  data = typeof data.data == 'undefined' ? {info: {}, items: []} : data.data
  console.log({order: data})
  return data
}

// 訂單取消
export function orderCancel(order_id = '', data = null) {
  if (order_id == '') return console.error('order_id is empty')
  if (data == null) return console.error('data is null')
  fetchData('put', 'orders/' + order_id, data, () => {
    $(document).find('.modal').modal('hide')
  })
}

// 退貨申請
export function reFunds(data = null) {
  if (data == null) return console.error('data is null')
  console.log({seRF: data})
  fetchData('post', 'refunds', data, () => {
    $(document).find('.modal').modal('hide')
  })
}

// 取得退貨進度列表
export function getRefundData() {
  let data = props() == null ? false : props()
  data = typeof data.refunds == 'undefined' ? {} : data.refunds
  console.log({getRefundData: data})
  return data
}

// 取得退貨進度內容
export function getRefundDetailData() {
  let data = props() == null ? false : props()
  data = typeof data.refund == 'undefined' ? {} : data.refund
  data = typeof data.data == 'undefined' ? {} : data.data
  console.log({getRefundDetailData: data})
  return data
}
