import {props, apiUrl, fetchData} from '@/Web/Config/api/request'
import {getCurrentInstance, computed, reactive} from 'vue'
import {getShop} from '@/Web/Config/api/global.js'
import { Inertia } from '@inertiajs/inertia'

// 取得購物車資料
export function getCartData() {
  let data = props() == null ? false : props()
  data = typeof data.cart == 'undefined' ? {} : data.cart
  data = typeof data.items == 'undefined' ? {} : data.items
  data = typeof data.data == 'undefined' ? [] : data.data
  let total = 0
  data.forEach((item, key) => {
    total += item.model.price
  })
  return {data: data, total: total}
}

// 取得購物車列表資料
export function getCartListData() {
  let data = props() == null ? false : props()
  data = typeof data.shoppingListing == 'undefined' ? {} : data.shoppingListing
  data = typeof data.data == 'undefined' ? [] : data.data
  let total = 0
  data.forEach((row, key) => {
    let subtotal = 0
    subtotal = row.items.reduce((a, b) => {
      const qty = b.qty <= 0 ? 1 : b.qty
      const special = parseInt(b.model.price.special)
      b.qty = qty
      b.selected = typeof b.selected == 'undefined' ? false : b.selected
      b.subtotal = (special * qty)
      return a + b.subtotal
    }, 0)
    row.isSelect = typeof row.isSelect == 'undefined' ? false : row.isSelect
    row.subtotal = subtotal
    total = total + subtotal
  })
  console.log({cart: data, total: total})
  return {data: data, total: total, isLogin: false}
}

// 加入購物車
export function addCartData(el, data) {
  let elData = el.data()
  let modal = el.parents('.modal')
  const id = typeof data.id == 'undefined' ? 0 : data.id
  const qty = typeof data.quantity == 'undefined' ? 0 : data.quantity

  fetchData('post', 'shopping/cart/items', {item: {id: id, qty: qty}}, () => {
    if (modal.length > 0) {
      modal.modal('toggle')
    }
    if (typeof elData.target !== 'undefined') {
      const target = elData.target
      if ($(document).find(target).length > 0) {
        $(document).find(target).modal()
      }
    }
  })
}

// 刪除購物車
export function delCartData(data) {
  let shopNo = getShop()
  shopNo = typeof shopNo.no !== 'undefined' ? shopNo.no : ''
  let identity = typeof data.identity !== 'undefined' ? data.identity : ''
  if (identity == '') return console.error(['刪除購物車缺少 identity', data])
  fetchData('post', 'shopping/cart/items/destroy/' + identity, null, r => {
    console.log(r)
  })
}

// 購物車數量更新
export function updataCartQty(data) {
  console.log({updataCartQty: data})
  let identity = typeof data.identity !== 'undefined' ? data.identity : ''
  if (identity == '') return console.error(['刪除購物車2缺少 identity', data])
  fetchData('put', 'shopping/cart/items/' + identity, {qty: data.qty}, r => {
    console.log(r)
  })
}

// 刪除購物車2
export function delCartData2(data) {
  let identity = typeof data.identity !== 'undefined' ? data.identity : ''
  if (identity == '') return console.error(['刪除購物車2缺少 identity', data])
  fetchData('delete', 'shopping/cart/items/' + identity)
}

// 送出
export function checkout(data, fun = null) {
  fetchData('post', 'shopping/cart/checkout/', {identities: data}, fun)
}

// 取得 checkout 資料
export function getCheckoutData() {
  let data = props() == null ? false : props()
  const user = typeof data.user == 'undefined' ? null : data.user
  data = typeof data.checkout == 'undefined' ? {} : data.checkout
  if (typeof data.items == 'undefined') data.items = {data: []}
  if (typeof data.items.data == 'undefined') data.items.data = []
  let paymentMehods = typeof data.paymentMehods == 'undefined' ? {data: []} : data.paymentMehods
  paymentMehods = typeof paymentMehods.data == 'undefined' ? [] : paymentMehods.data
  data.isRead = false
  data.user = user
  data.paymentMethod = paymentMehods.length > 0 ? paymentMehods[0]['id'] : 0
  data.consumer = {
    name: '', phone: '',
    email: '', password: '',
    password_confirmation: '',
  }
  data.invoice = {
    type: 'member',
    ext: {
      is_agree: false, code: '',
      title: '', tex_number: ''
    }
  }
  data.items.data.forEach(row => {
    row.note = ''
    row.shippingMethods = 2
    let adr = '', name = '', phone = ''
    if (user !== null) {
      if (typeof user.addressees !== 'undefined') {
        if (typeof user.addressees.data !== 'undefined') {
          if (Array.isArray(user.addressees.data)) {
            let filterData = user.addressees.data.filter(ee => ee.default)
            if (filterData.length > 0) filterData = filterData[0]
            adr = filterData.address
            name = filterData.name
            phone = filterData.phone
          }
        }
      }
    }
    row.recipient = {
      name: name,
      phone: phone,
      address: adr
    }
  })
  console.log({checkoutData: data})
  return data
}

// 新增店家
export function addShopData(data, fun = null) {
  fetchData('post', 'account/addressee', data, fun, ['user'])
}

// 更新店家
export function editShopData(id, data, fun = null) {
  fetchData('put', 'account/addressee/' + id, data, fun, ['user'])
}

// 送出資料
export function sendOrderData(data, fun = null) {
  fetchData('post', 'orders', data, fun, ['checkout'])
}