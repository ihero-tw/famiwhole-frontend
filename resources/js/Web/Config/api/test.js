import { getCurrentInstance } from 'vue'
import { Inertia } from '@inertiajs/inertia'

export function test1() {
  return {
    test: 123
  }
}

export function addCart(data = null) {
  // const app = getCurrentInstance()
  // const mixin = app.proxy
  let url = '/goodsupply/999999/shopping/cart/items'
  Inertia.form({
    item: {
      id: 'test2',
      qty: '2',
    },
  }).post(url, {
    preserveScroll: true,
    onSuccess: (ex) => {
      console.log('success', ex)
      $(document).find('#addToCart').modal('hide')
    }
  })
}