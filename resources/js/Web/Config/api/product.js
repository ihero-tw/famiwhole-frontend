import {props, apiUrl} from '@/Web/Config/api/request'
import {getCurrentInstance, computed, reactive} from 'vue'
import { Inertia } from '@inertiajs/inertia'

// 處理產品資料
function productData(data) {
  data.forms = {
    id: '', qty: 0,
    stock: 0, quantity: 0
  }

  // 處理規格
  let total = 0
  const specListLength = data.spec.list.length
  data.specData = []
  data.spec.list.forEach((item, key) => {
    const optionLength = item.options.length
    const isLastData = (optionLength - 1) == key ? true : false
    item.child = data.categories
    data.forms[item.name] = 0;
    // row.options.forEach((rs, rk) => {
    //   rs.model = data.models
    //   rs.modelActive  = (specListLength - 1) == key ? true : false
    //   let filterModel = data.models.filter(s => s.uuids.indexOf(rs.uuid))
    //   if (filterModel.length <= 0) return
    //   filterModel     = filterModel[0]
    //   rs.modelId      = filterModel.id
    //   rs.modelName    = filterModel.name
    //   rs.modelSku     = typeof filterModel.sku === 'undefined' ? '' : filterModel.sku
    //   rs.modelPrice   = Object.assign({}, filterModel.price)
    //   rs.stockSurplus = filterModel.stockSurplus
    // })

    item.options.forEach((row, rk) => {
      let uuids = ''
      if (key <= 0) uuids = row.uuid + '-'
      else if (key > 0 && key < (data.spec.list.length - 1)) uuids = '-' + row.uuid + '-'
      else uuids = '-' + row.uuid
      row.uuids = uuids
      row.stockSurplus = 0
      row.price        = data.price
      row.model        = data.models
      row.modelActive  = isLastData
      let filterModel  = data.models.filter(f => f.uuid.indexOf(uuids))
      row.filterModel  = []
      filterModel.map(m => {
        row.stockSurplus += m.stockSurplus
        row.filterModel.push(m);
      })
      // console.log([uuids, filterModel])
    })
    data.specData.push(item)
  })

  // 計算總庫存
  Object.assign([], data.models).forEach(item => total += item.stockSurplus)
  data.forms.stock = total
  return data
}

// 取得產品列表
export function getProduct() {
  let data = props() == null ? false : props()
  data = typeof data.products == 'undefined' ? {data: [], links: {}, meta: {}} : data.products
  console.log({type: 'product list', 'data': data})
  let dataItems = data.data
  dataItems.forEach((item, key) => {
    dataItems[key] = productData(item)
  })
  data.data = dataItems
  return data
}

// 取得單一產品內容
export function getProductDetail() {
  let data = props() == null ? false : props()
  data = typeof data.product == 'undefined' ? [] : data.product
  if (typeof data.data == 'undefined') return {}
  data = productData(data.data)
  // console.log({singleApiProduct: data})
  return data
}

// 取得相關產品
export function getRelateProduct() {
  let data = props() == null ? false : props()
  data = typeof data.related == 'undefined' ? [] : data.related
  return data
}