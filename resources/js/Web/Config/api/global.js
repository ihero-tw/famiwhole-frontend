import {props, apiUrl} from '@/Web/Config/api/request'
import {getCurrentInstance, computed, reactive} from 'vue'
import { Inertia } from '@inertiajs/inertia'

export function getMarket() {
  let data = props() == null ? false : props()
  data = typeof data.market == 'undefined' ? false : data.market
  if (typeof data == 'undefined') return {intro: '', name: '', no: ''}
  if (typeof data.data == 'undefined') return {intro: '', name: '', no: ''}
  data = data.data
  const shopData = getShop()
  data.shopName = shopData.name
  data.shopData = shopData
  console.log({'market': data, g: props()})
  return data
}

export function getMarketMenu() {
  let data = props() == null ? false : props()
  data = typeof data.markets == 'undefined' ? false : data.markets
  if (typeof data.data == 'undefined') return []
  data = data.data
  return data
}

export function getBanner() {
  let data = props() == null ? false : props()
  data = typeof data.banner == 'undefined' ? false : data.banner
  if (typeof data !== 'object') return []
  return data.data
}

export function getShop() {
  let data = props() == null ? false : props()
  data = typeof data.shop == 'undefined' ? false : data.shop
  if (!data.data) {
    return {
      address: '', email: '', icon: '',
      logo: '', name: '', no: '',
      phone: '', uri: '',
      socialMedias: {link: [], plugin: {type: '', uri: ''}}
    }
  }
  data = data.data
  data.addressCombine = data.address.city + data.address.town + data.address.street
  // console.log({shop: data})
  return data
}