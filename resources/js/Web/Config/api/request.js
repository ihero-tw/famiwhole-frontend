import { usePage } from '@inertiajs/inertia-vue3'
import { Inertia } from '@inertiajs/inertia'

export function props() {
  let data = usePage().props.value
  data = typeof data !== 'undefined' ? data : null
  return data
}

export function apiUrl() {
  let shop = props() == null ? false : props()
  shop = typeof shop.shop == 'undefined' ? false : shop.shop
  shop = typeof shop.data == 'undefined' ? false : shop.data
  let uri = typeof shop.uri == 'undefined' ? '' : shop.uri
  return uri
}

export function fetchData(method = 'post', url, data = null, callback, only = null) {
  let forms = Inertia
  if (data !== null) forms = forms.form(data)
  const api = apiUrl() + url
  console.log({type: 'fetch', method: method, api: api, data: data, inerForm: forms})
  let options = {
    preserveScroll: true,
    onSuccess: (ex) => {
      console.log('api success', ex)
      if (typeof callback === 'function') callback(ex)
    }
  }
  if (only !== null) options.only = only
  forms[method](api, options)
}

export function thisFun() {
  console.log({up: usePage()})
}