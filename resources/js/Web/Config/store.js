import {props, apiUrl} from '@/Web/Config/api/request'
import { reactive, ref } from 'vue'

let firstIn = true
const store = ({
  state: reactive({
    count: 0,
    cartData: {total: 0, data: []},
    tempData: {}
  }),
  settting(col, val) {
    if (typeof store.state[col] !== 'undefined') return false
    store.state[col] = val
  },
  clearCartData () {
    store.state.cartData = {total: 0, data: []}
    store.storgeDel('cart')
  },
  storgeSet: (key, value) => localStorage.setItem(key, JSON.stringify(value)),
  storgeGet: (key) => JSON.parse(localStorage.getItem(key)),
  storgeDel: (key) => localStorage.removeItem(key),
})

export default store