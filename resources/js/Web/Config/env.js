import {thisFun, props, apiUrl} from '@/Web/Config/api/request'
import { reactive, toRefs, computed } from "vue";
import { Inertia } from '@inertiajs/inertia'
import { addCartData } from '@/Web/Config/api/cart.js'

const method = {
  addCart(e, data) {
    if (data.id == '') return console.error(['cart id is null', data])
    if (data.quantity <= 0) return console.error(['cart quantity is 0', data])
    addCartData($(e.currentTarget), data)
  },
  showModalCart(e, data) {
    let el = $(e.currentTarget)
    let elData = el.data()
    if (typeof elData.target === 'undefined') return false
    const target = elData.target
    if ($(document).find(target).length <= 0) return false
    $(document).find('.modal').modal('hide')
    this.store.state.tempData = data
    $(document).find(target).modal('show')
    console.log({
      type: 'minix show cart',
      el: el,
      elData: elData,
      data: data,
      'this': this.store
    })
  },
  tPrice(data) {
    if (typeof data == 'undefined') return ''
    if (typeof data == 'string') return data
    return `<span class="old-price">$${data.special}</span>
      <span class="new-price">
        <small>$</small> ${data.original}
      </span>`
  },
  setActive(data = null) {
    return typeof data.id !== 'undefined' ? data.id : 0
  },
  getSpecChild(data) {
    const tempData = this.store.state.tempData
    console.log(['sch', data, tempData])
    if (typeof data == 'undefined') return []
    if (typeof tempData.forms == 'undefined') return []
    let fs = data.filter(item => item.name == tempData.forms.spec)
    console.log({specs: data, select: tempData, filter: fs})
    if (fs.length == 0) return []
    return fs[0]['options']
  },
  getParams(search = '') {
    search = search == '' ? location.search.substring(1) : search;
    const s1 = decodeURI(search).replace(/"/g, '\\"')
    const s2 = s1.replace(/&/g, '","')
    const s3 = s2.replace(/=/g,'":"')
    const s4 = '{"' + s3 + '"}';
    // console.log({gParams: s4})
    if (!this.isJson(s4)) return {}
    return JSON.parse(s4)
  },
  isJson(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }
}

const propData = {
  shop: {
    type: Object,
    default() {
      return {no: '', name: ''}
    }
  },
}

export default {
  methods: method,
  computed: {
    baseUrl() {
      return apiUrl()
    },
  },
  props: propData
}