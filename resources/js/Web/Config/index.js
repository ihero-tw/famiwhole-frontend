// import components
import { App as InertiaApp, plugin as InertiaPlugin } from '@inertiajs/inertia-vue3';
import { Head, Link } from '@inertiajs/inertia-vue3';
import VueSweetalert2 from 'vue-sweetalert2';

// custom plugin
import Env from './env'
import Store from './store'
import Panigation from './Panigation/index';
import Navigation from './Navigation/index';
import Img from './Img/index';
import Facebook from './Facebook/index';
import rHtml from './rHtml';
import numInput from './numInput';
import flowChart from './FlowChart';

// register data list
const installComponentData = {
  'directive': [
    {'name': 'rHtml', 'component': rHtml},
  ],
  'component': [
    {'name': 'InertiaHead', 'component': Head},
    {'name': 'InertiaLink', 'component': Link},
    {'name': 'Navigation', 'component': Navigation},
    {'name': 'Img', 'component': Img},
    {'name': 'Facebook', 'component': Facebook},
    {'name': 'Panigation', 'component': Panigation},
    {'name': 'numInput', 'component': numInput},
    {'name': 'flowChart', 'component': flowChart},
  ],
  'use': [
    {'component': InertiaPlugin},
    {'component': VueSweetalert2},
  ],
  'mixin': [
    {'component': Env},
  ]
}

// register all components
export default{
  install (app) {
    app.config.globalProperties.store = Store

    Object.keys(installComponentData).forEach(dataItem => {
      installComponentData[dataItem].forEach(item => {
        if (['use', 'mixin'].indexOf(dataItem) >= 0) {
          app[dataItem](item.component)
        } else {
          app[dataItem](item.name, item.component)
        }
      })
    })
  }
}