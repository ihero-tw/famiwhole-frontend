function transitionTextType(text) {
  if (typeof text == 'object') {
    text = JSON.stringify(text)
  }
  return text
}

export default (el, binding, vnode) => {
  const arg = binding.arg
  let value = transitionTextType(binding.value)
  if (arg == 'text') {
    el.innerText = value
  } else {
    el.innerHTML = value
  }
  // console.log({
  //   'type': 'directive - ' + typeof value,
  //   'el': el,
  //   'bind': binding,
  //   'node': vnode,
  //   'arg': arg,
  //   'value': binding.value
  // })
}